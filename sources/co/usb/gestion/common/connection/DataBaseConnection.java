/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.common.connection;

import java.sql.Connection;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class DataBaseConnection {

    private static DataBaseConnection instancia = null;

    /**
     * Creates a new instance of DataBaseConnection
     */
    public DataBaseConnection() {
    }

    /**
     *
     * @return
     */
    public static synchronized DataBaseConnection getInstance() {

        if (instancia == null) {
            instancia = new DataBaseConnection();
        }

        return instancia;

    }

    /**
     *
     * @param contextDataResourceName
     * @return
     * @throws Exception
     */
    public Connection getConnection(String contextDataResourceName) throws Exception {

        Context initCtx = null;
        Context envCtx = null;
        DataSource ds = null;
        Connection con = null;

        try {

            initCtx = new InitialContext();
            envCtx = (Context) initCtx.lookup("java:comp/env");
            ds = (DataSource) envCtx.lookup(contextDataResourceName);
            con = ds.getConnection();

        } finally {
        }
        return con;
    }
}
