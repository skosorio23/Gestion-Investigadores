/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.common.util;

/**
 *
 * @author Sys. Hebert Medelo
 */
public interface Generales {

    /**
     * Identifica una cadena vacia
     */
    public static final String EMPTYSTRING = "";
    /**
     * Identifica una cadena nula
     */
    public static final String NULLVALUE = null;
    /**
     * Identifica una cadena de valor 0
     */
    public static final String ZERO = "0";
    /**
     * Identifica un tipo de Fecha estandar
     */
    public static final int DATE_FECHA = 1;
    /**
     * Identifica una Fecha tipo Fecha-Hora
     */
    public static final int DATE_FECHA_HORA = 2;
    /**
     * Identifica una Fecha tipo YY-MM-DD
     */
    public static final int DATE_FECHA_YY_MM_DD = 3;
    /**
     * Identifica una Fecha de tipo TIMESTAMP SQL
     */
    public static final int DATE_FECHA_HORA_SQL = 4;
    /**
     * Identifica una Fecha de tipo DD/MM/YYYY
     */
    public static final int FECHA_MOSTRAR = 5;
    /**
     * Identifica un int de valor 0
     */
    public static final int ZEROVALUE = 0;
    /**
     * Identifica un rango de un mes 31 dias
     */
    public static final int FECHA_UN_MES = 30;
    /**
     * Identifica un rango de dos meses 62 dias
     */
    public static final int FECHA_DOS_MESES = 60;
    /**
     * Identifica un rango de 45 dias
     */
    public static final int FECHA_OBLIGA_CAMBIO_CLAVE = 45;

    public static final boolean ESTADO_INICIAL = true;
}
