/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.constantes.Constantes;
import co.usb.gestion.mvc.dto.FormacionAcademicalDTO;
import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.dto.UsuarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class FormacionAcademicalDAO {


    /**
     *
     * @param conexion
     * @param formacion
     * @return
     */
    public boolean registrarFormacion(Connection conexion, FormacionAcademicalDTO formacion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO formacion_academica (foac_anioinicio, foac_aniograduacion, foac_institucion, foac_programaacademico, foac_estado, nifo_id, perf_id) ");
            cadSQL.append(" VALUES (?, ?, ?, ?, ?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, formacion.getFecha(), ps);
            AsignaAtributoStatement.setString(2, formacion.getGraduacion(), ps);
            AsignaAtributoStatement.setString(3, formacion.getInstitucion(), ps);
            AsignaAtributoStatement.setString(4, formacion.getPrograma(), ps);
            AsignaAtributoStatement.setString(5, Constantes.ESTADO_ACTIVO, ps);
            AsignaAtributoStatement.setString(6, formacion.getNivel(), ps);
            AsignaAtributoStatement.setString(7, formacion.getPerfilID(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    formacion.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }
    
    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarFormacionAcad(Connection conexion, FormacionAcademicalDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE formacion_academica SET foac_anioinicio = ?, foac_aniograduacion = ?, foac_institucion = ?,"
                        + " foac_programaacademico = ?, nifo_id = ?, perf_id = ? WHERE foac_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getFecha(), ps);
            AsignaAtributoStatement.setString(2, datos.getGraduacion(), ps);
            AsignaAtributoStatement.setString(3, datos.getInstitucion(), ps);
            AsignaAtributoStatement.setString(4, datos.getPrograma(), ps);
            AsignaAtributoStatement.setString(5, datos.getNivel(), ps);
            AsignaAtributoStatement.setString(6, datos.getPerfilID(), ps);
            AsignaAtributoStatement.setString(7, datos.getId(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorInstitucionPrograma(Connection conexion, String condicion, String perfil) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<FormacionAcademicalDTO> listado = null;
        FormacionAcademicalDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT f.foac_id,f.foac_anioinicio,f.foac_aniograduacion,f.foac_institucion,f.foac_programaacademico," );
            cadSQL.append("       n.nifo_id,n.nifo_descripcion,p.perf_id,p.perf_nombre" );
            cadSQL.append(" FROM formacion_academica f INNER JOIN nivel_formacion n ON f.nifo_id = n.nifo_id" );
            cadSQL.append("  INNER JOIN perfil p ON f.perf_id = p.perf_id" );
            cadSQL.append("  INNER JOIN usuario u ON p.usua_id = u.usua_id" );
            cadSQL.append("  INNER JOIN tipo_usuario t ON u.tius_id = t.tius_id" );
            cadSQL.append("  WHERE p.perf_id = "+ perfil +" AND f.foac_estado = ? "
                    + "AND t.tius_descripcion = 'Investigador' AND CONCAT_WS('',f.foac_institucion,f.foac_programaacademico)like '%" + condicion + "%' ");
         
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new FormacionAcademicalDTO();
                datos.setId(rs.getString("f.foac_id"));
                datos.setFecha(rs.getString("f.foac_anioinicio"));
                datos.setGraduacion(rs.getString("f.foac_aniograduacion"));
                datos.setInstitucion(rs.getString("f.foac_institucion"));
                datos.setPrograma(rs.getString("f.foac_programaacademico"));
                datos.setNivelID(rs.getString("n.nifo_id"));
                datos.setNivel(rs.getString("n.nifo_descripcion"));
                datos.setPerfilID(rs.getString("p.perf_id"));
                datos.setPerfil(rs.getString("p.perf_nombre"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
    
    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorInstitucionProgramaOperacional(Connection conexion, String condicion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<FormacionAcademicalDTO> listado = null;
        FormacionAcademicalDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT f.foac_id,f.foac_anioinicio,f.foac_aniograduacion,f.foac_institucion,f.foac_programaacademico," );
            cadSQL.append("       n.nifo_id,n.nifo_descripcion,p.perf_id,p.perf_nombre" );
            cadSQL.append(" FROM formacion_academica f INNER JOIN nivel_formacion n ON f.nifo_id = n.nifo_id" );
            cadSQL.append("  INNER JOIN perfil p ON f.perf_id = p.perf_id" );
            cadSQL.append("  INNER JOIN usuario u ON p.usua_id = u.usua_id" );
            cadSQL.append("  INNER JOIN tipo_usuario t ON u.tius_id = t.tius_id" );
            cadSQL.append("  WHERE t.tius_descripcion = 'Investigador' AND CONCAT_WS('',f.foac_institucion,f.foac_programaacademico)like '%" + condicion + "%' AND foac_estado = ? ");
         
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new FormacionAcademicalDTO();
                datos.setId(rs.getString("f.foac_id"));
                datos.setFecha(rs.getString("f.foac_anioinicio"));
                datos.setGraduacion(rs.getString("f.foac_aniograduacion"));
                datos.setInstitucion(rs.getString("f.foac_institucion"));
                datos.setPrograma(rs.getString("f.foac_programaacademico"));
                datos.setNivelID(rs.getString("n.nifo_id"));
                datos.setNivel(rs.getString("n.nifo_descripcion"));
                datos.setPerfilID(rs.getString("p.perf_id"));
                datos.setPerfil(rs.getString("p.perf_nombre"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
    
    
     /**
      * 
      * @param conexion
      * @param perfil
      * @return 
      */
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorIdPerfil(Connection conexion,String perfil) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<FormacionAcademicalDTO> listado = null;
        FormacionAcademicalDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT f.foac_id,f.foac_anioinicio,f.foac_aniograduacion,f.foac_institucion,f.foac_programaacademico, ");
            cadSQL.append(" n.nifo_id,n.nifo_descripcion,p.perf_id,p.perf_nombre ");
            cadSQL.append(" FROM formacion_academica f ");
            cadSQL.append(" INNER JOIN nivel_formacion n ON f.nifo_id = n.nifo_id  ");
            cadSQL.append(" INNER JOIN perfil p ON f.perf_id = p.perf_id ");
            cadSQL.append(" WHERE f.perf_id = ? AND foac_estado = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, perfil, ps);
            AsignaAtributoStatement.setString(2, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new FormacionAcademicalDTO();
                datos.setId(rs.getString("f.foac_id"));
                datos.setFecha(rs.getString("f.foac_anioinicio"));
                datos.setGraduacion(rs.getString("f.foac_aniograduacion"));
                datos.setInstitucion(rs.getString("f.foac_institucion"));
                datos.setPrograma(rs.getString("f.foac_programaacademico"));
                datos.setNivelID(rs.getString("n.nifo_id"));
                datos.setNivel(rs.getString("n.nifo_descripcion"));
                datos.setPerfilID(rs.getString("p.perf_id"));
                datos.setPerfil(rs.getString("p.perf_nombre"));
                
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
    
     /**
      * 
      * @param conexion
      * @param perfil
      * @return 
      */
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorIdPerfilTemporal(Connection conexion,String perfil) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<FormacionAcademicalDTO> listado = null;
        FormacionAcademicalDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tm.nifo_id ");
            cadSQL.append(" FROM temporal tm ");
            cadSQL.append(" WHERE tm.perf_id = ? AND tm.nifo_id IS NOT NULL AND tm.temp_estado = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, perfil, ps);
            AsignaAtributoStatement.setString(2, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                 System.out.println("dentro del while ");
                datos = new FormacionAcademicalDTO();
                datos.setNivelID(rs.getString("tm.nifo_id"));
                System.out.println("nivel formacion dao "+ datos.toStringJson() );
                
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
    
    /**
     *
     * @param conexion
     * @param formacion
     * @return
     */
    public boolean SimulacionregistrarFormacion(Connection conexion, FormacionAcademicalDTO formacion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO temporal (nifo_id, perf_id,temp_cantidad,temp_estado) ");
            cadSQL.append(" VALUES (?, ?,'1','1') ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, formacion.getNivel(), ps);
            AsignaAtributoStatement.setString(2, formacion.getPerfilID(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    formacion.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }
    
    
    
//    /**
//     * 
//     * @param conexion
//     * @param perfil
//     * @return 
//     */
//    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorIdPerfil(Connection conexion, String perfil) {
//        System.out.println("llego a al DAO");
//        System.out.println("perfil  " + perfil);
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ArrayList<FormacionAcademicalDTO> listado = null;
//        FormacionAcademicalDTO datos = null;
//        StringBuilder cadSQL = null;
//
//        try {
//            cadSQL = new StringBuilder();
//            cadSQL.append(" SELECT f.foac_id,f.foac_anioinicio,f.foac_aniograduacion,f.foac_institucion,f.foac_programaacademico," );
//            cadSQL.append(" n.nifo_id,n.nifo_descripcion,p.perf_id,p.perf_nombre" );
//            cadSQL.append(" FROM formacion_academica f " );
//            cadSQL.append(" INNER JOIN nivel_formacion n ON f.nifo_id = n.nifo_id" );
//            cadSQL.append(" INNER JOIN perfil p ON f.perf_id = p.perf_id" );
//            cadSQL.append(" WHERE f.perf_id = ?");
//         
//            ps = conexion.prepareStatement(cadSQL.toString());
//            AsignaAtributoStatement.setString(1, perfil, ps);
//            rs = ps.executeQuery();
//            listado = new ArrayList();
//            System.out.println(" por aquiiiiiiiiiiiiiiiiiiiiiiii ");
//
//            while (rs.next()) {
//                System.out.println("dentro del while ");
//                datos = new FormacionAcademicalDTO();
//                datos.setId(rs.getString("f.foac_id"));
//                datos.setFecha(rs.getString("f.foac_anioinicio"));
//                datos.setGraduacion(rs.getString("f.foac_aniograduacion"));
//                datos.setInstitucion(rs.getString("f.foac_institucion"));
//                datos.setPrograma(rs.getString("f.foac_programaacademico"));
//                datos.setNivelID(rs.getString("n.nifo_id"));
//                datos.setNivel(rs.getString("n.nifo_descripcion"));
//                datos.setPerfilID(rs.getString("p.perf_id"));
//                datos.setPerfil(rs.getString("p.perf_nombre"));
//                
//                System.out.println("nivel formacion dao "+ datos.toStringJson() );
//                
//                listado.add(datos);
//            }
//            ps.close();
//            ps = null;
//        } catch (Exception e) {
//            LoggerMessage.getInstancia().loggerMessageException(e);
//            return null;
//        } finally {
//            try {
//                if (ps != null) {
//                    ps.close();
//                    ps = null;
//                }
//                if (listado != null && listado.isEmpty()) {
//                    listado = null;
//                }
//               
//            } catch (Exception e) {
//                LoggerMessage.getInstancia().loggerMessageException(e);
//                return null;
//            }
//        }
//        System.out.println("listado "  + listado);
//        return listado;
//    }

   
}
