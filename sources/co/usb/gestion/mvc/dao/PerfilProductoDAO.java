/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.constantes.Constantes;
import co.usb.gestion.mvc.dto.PerfilProductoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class PerfilProductoDAO {

    /**
     *
     * @param conexion
     * @param Datos
     * @return
     */
    public boolean registrarPerfilProducto(Connection conexion, PerfilProductoDTO Datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO perfil_producto (supr_id, perf_id, supr_nombre, supr_descripcion, pepr_estado, supr_fecha, supr_clase, supr_url, pepr_clase) ");
            cadSQL.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, Datos.getIdSubtipoProducto(), ps);
            AsignaAtributoStatement.setString(2, Datos.getIdPerfil(), ps);
            AsignaAtributoStatement.setString(3, Datos.getNombre(), ps);
            AsignaAtributoStatement.setString(4, Datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(5, Constantes.ESTADO_ACTIVO, ps);
            AsignaAtributoStatement.setString(6, Datos.getFecha(), ps);
            AsignaAtributoStatement.setString(7, Datos.getClaseproducto(), ps);
            AsignaAtributoStatement.setString(8, Datos.getUrl(), ps);
            AsignaAtributoStatement.setString(9, Datos.getClaseproducto(), ps);
            
            System.out.println(cadSQL.toString()+":"+Datos.getClaseproducto());

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    Datos.setIdPerfilProducto(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarPerfilProducto(Connection conexion, PerfilProductoDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE perfil_producto SET supr_id = ?, perf_id = ?, supr_nombre = ?,"
                    + " supr_descripcion = ?, supr_fecha = ?, supr_clase = ?, supr_url = ?, pepr_clase = ? WHERE pepr_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdSubtipoProducto(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdPerfil(), ps);
            AsignaAtributoStatement.setString(3, datos.getNombre(), ps);
            AsignaAtributoStatement.setString(4, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(5, datos.getFecha(), ps);
            AsignaAtributoStatement.setString(6, datos.getClaseproducto(), ps);
            AsignaAtributoStatement.setString(7, datos.getUrl(), ps);
            AsignaAtributoStatement.setString(8, datos.getIdPerfilProducto(), ps);
            AsignaAtributoStatement.setString(8, datos.getClaseproducto(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<PerfilProductoDTO> consultarSubtipo(Connection conexion, String condicion, String perfil) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilProductoDTO> listado = null;
        PerfilProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();

            cadSQL.append(" SELECT pp.pepr_id,pp.supr_id,sp.supr_descripcion,pp.supr_nombre,pp.supr_descripcion,pp.supr_fecha,pp.supr_clase,pp.supr_url");
            cadSQL.append("  FROM perfil p INNER JOIN perfil_producto pp ON p.perf_id = pp.perf_id");
            cadSQL.append(" INNER JOIN subtipo_producto sp ON pp.supr_id = sp.supr_id");
            cadSQL.append("  INNER JOIN tipo_producto tp ON sp.tipr_id = tp.tipr_id");
            cadSQL.append("  INNER JOIN clase_producto cp ON sp.supr_id = cp.supr_id");
            cadSQL.append("  WHERE p.perf_id = " + perfil + " AND pp.pepr_estado = ? GROUP BY 1");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilProductoDTO();
                datos.setIdPerfilProducto(rs.getString("pp.pepr_id"));
                datos.setIdSubtipoProducto(rs.getString("pp.supr_id"));
                datos.setDescripcionSubtipoProd(rs.getString("sp.supr_descripcion"));
                datos.setNombre(rs.getString("pp.supr_nombre"));
                datos.setDescripcion(rs.getString("pp.supr_descripcion"));
                datos.setFecha(rs.getString("pp.supr_fecha"));
                datos.setClaseproducto(rs.getString("pp.supr_clase"));
                datos.setUrl(rs.getString("pp.supr_url"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
    
    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<PerfilProductoDTO> consultarSubtipoOperacional(Connection conexion, String condicion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilProductoDTO> listado = null;
        PerfilProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();

            cadSQL.append(" SELECT pp.pepr_id,pp.supr_id,sp.supr_descripcion,pp.supr_nombre,pp.supr_descripcion,pp.supr_fecha,pp.supr_clase,pp.supr_url,p.perf_id");
            cadSQL.append("  FROM perfil p INNER JOIN perfil_producto pp ON p.perf_id = pp.perf_id");
            cadSQL.append(" INNER JOIN subtipo_producto sp ON pp.supr_id = sp.supr_id");
            cadSQL.append("  INNER JOIN tipo_producto tp ON sp.tipr_id = tp.tipr_id");
            cadSQL.append("  INNER JOIN clase_producto cp ON sp.supr_id = cp.supr_id");
            cadSQL.append(" WHERE pp.pepr_estado = ? ");
            cadSQL.append("  GROUP BY 1");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilProductoDTO();
                datos.setIdPerfilProducto(rs.getString("pp.pepr_id"));
                datos.setIdSubtipoProducto(rs.getString("pp.supr_id"));
                datos.setDescripcionSubtipoProd(rs.getString("sp.supr_descripcion"));
                datos.setNombre(rs.getString("pp.supr_nombre"));
                datos.setDescripcion(rs.getString("pp.supr_descripcion"));
                datos.setFecha(rs.getString("pp.supr_fecha"));
                datos.setClaseproducto(rs.getString("pp.supr_clase"));
                datos.setUrl(rs.getString("pp.supr_url"));
                datos.setIdPerfil(rs.getString("p.perf_id"));
                listado.add(datos);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    
    /**
     *
     * @param conexion
     * @param idPerfil
     * @return
     */
    public ArrayList<PerfilProductoDTO> ConsultarPerfilProducto(Connection conexion, String idPerfil) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilProductoDTO> listado = null;
        PerfilProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            
            cadSQL.append(" SELECT pepr.pepr_id, pepr.supr_id, pepr.perf_id, pepr.supr_nombre, pepr.supr_descripcion, pepr.supr_fecha,pepr.supr_clase,pepr.supr_url, ");
            cadSQL.append(" supr.supr_descripcion, supr.supr_estado, ");
            cadSQL.append(" tipr.tipr_id, tipr.tipr_descripcion, tipr.tipr_estado ");
            cadSQL.append(" FROM perfil_producto as pepr ");
            cadSQL.append(" INNER JOIN subtipo_producto AS supr ON pepr.supr_id = supr.supr_id");
            cadSQL.append(" INNER JOIN tipo_producto AS tipr ON supr.tipr_id = tipr.tipr_id ");
            cadSQL.append(" WHERE pepr.perf_id = ? AND pepr.pepr_estado = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idPerfil, ps);
            AsignaAtributoStatement.setString(2, Constantes.ESTADO_ACTIVO, ps);

            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilProductoDTO();
                datos.setIdPerfilProducto(rs.getString("pepr.pepr_id"));
                datos.setIdSubtipoProducto(rs.getString("pepr.supr_id"));
                datos.setIdPerfil(rs.getString("pepr.perf_id"));
                datos.setNombre(rs.getString("pepr.supr_nombre"));
                datos.setDescripcion(rs.getString("pepr.supr_descripcion"));
                datos.setFecha(rs.getString("pepr.supr_fecha"));
                datos.setDescripcionSubtipoProd(rs.getString("supr.supr_descripcion"));
                datos.setEstadoSubtipoProd(rs.getString("supr.supr_estado"));
                datos.setIdTipoProducto(rs.getString("tipr.tipr_id"));
                datos.setDescripcionTipoProd(rs.getString("tipr.tipr_descripcion"));
                datos.setEstadoTipoProd(rs.getString("tipr.tipr_estado"));
                datos.setClaseproducto(rs.getString("pepr.supr_clase"));
                datos.setClaseproducto(rs.getString("pepr.supr_url"));
                listado.add(datos);
            }

            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

   /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<PerfilProductoDTO> listadoPerfilProducto(Connection conexion) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilProductoDTO> listado = null;
        PerfilProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT pepr.pepr_id, pepr.supr_id, pepr.perf_id, pepr.supr_nombre, pepr.supr_descripcion, pepr.supr_fecha,pepr.supr_clase, ");
            cadSQL.append(" supr.supr_descripcion, supr.supr_estado, ");
            cadSQL.append(" tipr.tipr_id, tipr.tipr_descripcion, tipr.tipr_estado ");
            cadSQL.append(" FROM perfil_producto as pepr ");
            cadSQL.append(" INNER JOIN subtipo_producto AS supr ON pepr.supr_id = supr.supr_id");
            cadSQL.append(" INNER JOIN tipo_producto AS tipr ON supr.tipr_id = tipr.tipr_id ");
            cadSQL.append(" WHERE pepr.pepr_estado = ? "); 

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilProductoDTO();
                datos.setIdPerfilProducto(rs.getString("pepr.pepr_id"));
                datos.setIdSubtipoProducto(rs.getString("pepr.supr_id"));
                datos.setIdPerfil(rs.getString("pepr.perf_id"));
                datos.setNombre(rs.getString("pepr.supr_nombre"));
                datos.setDescripcion(rs.getString("pepr.supr_descripcion"));
                datos.setFecha(rs.getString("pepr.supr_fecha"));
                datos.setDescripcionSubtipoProd(rs.getString("supr.supr_descripcion"));
                datos.setEstadoSubtipoProd(rs.getString("supr.supr_estado"));
                datos.setIdTipoProducto(rs.getString("tipr.tipr_id"));
                datos.setDescripcionTipoProd(rs.getString("tipr.tipr_descripcion"));
                datos.setEstadoTipoProd(rs.getString("tipr.tipr_estado"));
                datos.setClaseproducto(rs.getString("pepr.supr_clase"));
                listado.add(datos);
            }

            ps.close();
            ps = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param condicion
     * @return
     */
    public ArrayList<PerfilProductoDTO> consultarPerfilProductoXCondicion(Connection conexion, String condicion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<PerfilProductoDTO> listado = null;
        PerfilProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT pepr.pepr_id, pepr.supr_id, pepr.perf_id, pepr.supr_nombre, pepr.supr_descripcion, pepr.supr_fecha,pepr.supr_clase, ");
            cadSQL.append(" supr.supr_descripcion, supr.supr_estado, ");
            cadSQL.append(" tipr.tipr_id, tipr.tipr_descripcion, tipr.tipr_estado ");
            cadSQL.append(" FROM perfil_producto as pepr ");
            cadSQL.append(" INNER JOIN subtipo_producto AS supr ON pepr.supr_id = supr.supr_id");
            cadSQL.append(" INNER JOIN tipo_producto AS tipr ON supr.tipr_id = tipr.tipr_id ");
            cadSQL.append(" WHERE concat_ws(' ', pepr.supr_nombre, pepr.supr_descripcion ) like '%" + condicion + "%' AND pepr.pepr_estado = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new PerfilProductoDTO();
                datos.setIdPerfilProducto(rs.getString("pepr.pepr_id"));
                datos.setIdSubtipoProducto(rs.getString("pepr.supr_id"));
                datos.setIdPerfil(rs.getString("pepr.perf_id"));
                datos.setNombre(rs.getString("pepr.supr_nombre"));
                datos.setDescripcion(rs.getString("pepr.supr_descripcion"));
                datos.setFecha(rs.getString("pepr.supr_fecha"));
                datos.setDescripcionSubtipoProd(rs.getString("supr.supr_descripcion"));
                datos.setEstadoSubtipoProd(rs.getString("supr.supr_estado"));
                datos.setIdTipoProducto(rs.getString("tipr.tipr_id"));
                datos.setDescripcionTipoProd(rs.getString("tipr.tipr_descripcion"));
                datos.setEstadoTipoProd(rs.getString("tipr.tipr_estado"));
                datos.setClaseproducto(rs.getString("pepr.supr_clase"));
                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }
    
     /**
     *
     * @param conexion
     * @param idPerfil
     * @param idTipoProducto
     * @param clase
     * @return
     */
    public int consultarProductosPorTipoProductoYClase(Connection conexion, String idPerfil, String idTipoProducto, String clase) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        int productos = 0;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT count(*) AS cantidad ");
            cadSQL.append(" from perfil_producto p ");
            cadSQL.append(" inner join subtipo_producto s ON p.supr_id = s.supr_id ");
            cadSQL.append(" where p.perf_id = ? and s.tipr_id=? and pepr_clase=? AND p.pepr_estado = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idPerfil, ps);
            AsignaAtributoStatement.setString(2, idTipoProducto, ps);
            AsignaAtributoStatement.setString(3, clase, ps);
            AsignaAtributoStatement.setString(4, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                productos = (rs.getInt("cantidad"));
            }

            ps.close();
            ps = null;
        } catch (SQLException e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return productos;
    }
    
      /**
     *
     * @param conexion
     * @param idPerfil
     * @param idTipoProducto
     * @param clase
     * @return
     */
    public int consultarProductosPorTipoProductoYClaseTemporal(Connection conexion, String idPerfil, String idTipoProducto, String clase) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        int productos = 0;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT sum(tm.temp_cantidad) cantidad");
            cadSQL.append(" from temporal tm ");
            //cadSQL.append(" inner join subtipo_producto s ON tm.supr_id = s.supr_id ");
            cadSQL.append(" where tm.perf_id = ? and tm.tipr_id=? and tm.temp_grupoclase=? AND tm.temp_estado = ?");

            
            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idPerfil, ps);
            AsignaAtributoStatement.setString(2, idTipoProducto, ps);
            AsignaAtributoStatement.setString(3, clase, ps);
            AsignaAtributoStatement.setString(4, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            System.out.println("ssss"+cadSQL.toString()+"C:"+clase+"P:"+idPerfil+"T:"+idTipoProducto);
            if (rs.next()) {
                productos = (rs.getInt("cantidad"));
            }

            ps.close();
            ps = null;
        } catch (SQLException e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return productos;
    }
    
    
    
     /**
      * 
      * @param conexion
      * @param idPerfil
      * @param idsubTipoProducto
      * @return 
      */
    public int consultarSubtipoPorId(Connection conexion, String idPerfil, String idsubTipoProducto) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        int productos = 0;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT count(*) AS cantidad ");
            cadSQL.append(" from perfil_producto  ");
            cadSQL.append(" where perf_id = ? and supr_id=? AND pepr_estado = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idPerfil, ps);
            AsignaAtributoStatement.setString(2, idsubTipoProducto, ps);
            AsignaAtributoStatement.setString(3, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                productos = (rs.getInt("cantidad"));
            }

            ps.close();
            ps = null;
        } catch (SQLException e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return productos;
    }
    
     /**
      * 
      * @param conexion
      * @param idPerfil
      * @param idsubTipoProducto
      * @return 
      */
    public int consultarSubtipoPorIdTemporal(Connection conexion, String idPerfil, String idsubTipoProducto) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;
        int productos = 0;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT sum(tm.temp_cantidad) cantidad ");
            cadSQL.append(" from temporal tm  ");
            cadSQL.append(" where tm.perf_id = ? and tm.supr_id=? tm.temp_estado = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idPerfil, ps);
            AsignaAtributoStatement.setString(2, idsubTipoProducto, ps);
            AsignaAtributoStatement.setString(3, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                productos = (rs.getInt("cantidad"));
            }

            ps.close();
            ps = null;
        } catch (SQLException e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return productos;
    }
    
     /**
     *
     * @param conexion
     * @param Datos
     * @return
     */
    public boolean registrarPerfilProductoSimulacion(Connection conexion, PerfilProductoDTO Datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO temporal (temp_cantidad,temp_tiempo,supr_id,tipr_id,temp_grupoclase,perf_id, pepr_estado ) ");
            //cadSQL.append(" INSERT INTO temporal (temp_cantidad,temp_tiempo,supr_id,tipr_id,perf_id ) ");
            cadSQL.append(" VALUES (?, ?, ?, ?, ?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, Datos.getCantidad(), ps);
            AsignaAtributoStatement.setString(2, Datos.getTiempo(), ps);
            AsignaAtributoStatement.setString(3, Datos.getIdSubtipoProducto(), ps);
            AsignaAtributoStatement.setString(4, Datos.getIdTipoProducto(), ps);
            AsignaAtributoStatement.setString(5, Datos.getClaseproducto(), ps);
            AsignaAtributoStatement.setString(6, Datos.getIdPerfil(), ps);
            AsignaAtributoStatement.setString(7, Constantes.ESTADO_ACTIVO, ps);
            
            System.out.println(cadSQL.toString());

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    Datos.setIdPerfilProducto(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }


}
