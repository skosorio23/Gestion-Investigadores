/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.constantes.Constantes;
import co.usb.gestion.mvc.dto.SubtipoProductoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class SubtipoProductoDAO {

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<SubtipoProductoDTO> listarSubtipoProducto(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        SubtipoProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT s.supr_id, s.supr_descripcion, s.supr_estado, s.tipr_id, t.tipr_descripcion FROM subtipo_producto as s ");
            cadSQL.append(" INNER JOIN tipo_producto t ON t.tipr_id = s.tipr_id WHERE t.supr_estado = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new SubtipoProductoDTO();
                datos.setId(rs.getString("s.supr_id"));
                datos.setDescripcion(rs.getString("s.supr_descripcion"));
                datos.setEstado(rs.getString("s.supr_estado"));
                datos.setIdTipoProducto(rs.getString("s.tipr_id"));
                datos.setTipoProducto(rs.getString("t.tipr_descripcion"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarSubtipoProducto(Connection conexion, SubtipoProductoDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO subtipo_producto (supr_descripcion, supr_estado, tipr_id ) ");
            cadSQL.append(" VALUES (?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(2, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdTipoProducto(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    datos.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean actualizarSubipoProducto(Connection conexion, SubtipoProductoDTO datos) {

        PreparedStatement ps = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE subtipo_producto SET supr_descripcion = ?, supr_estado = ?, tipr_id = ? WHERE supr_id = ? ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(2, datos.getEstado(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdTipoProducto(), ps);
            AsignaAtributoStatement.setString(4, datos.getId(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                registroExitoso = true;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     * @param idProducto
     * @param conexion
     * @return
     */
    public ArrayList<SubtipoProductoDTO> listarSubtipoProductoPorIdProducto(Connection conexion, String idProducto) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList listado = null;
        SubtipoProductoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT s.supr_id, s.supr_descripcion, s.supr_estado, s.tipr_id, t.tipr_descripcion FROM subtipo_producto as s ");
            cadSQL.append(" INNER JOIN tipo_producto t ON t.tipr_id = s.tipr_id  ");
            cadSQL.append(" WHERE s.tipr_id = ? AND s.supr_estado = ? ");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idProducto, ps);
            AsignaAtributoStatement.setString(2, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new SubtipoProductoDTO();
                datos.setId(rs.getString("s.supr_id"));
                datos.setDescripcion(rs.getString("s.supr_descripcion"));
                datos.setEstado(rs.getString("s.supr_estado"));
                datos.setIdTipoProducto(rs.getString("s.tipr_id"));
                datos.setTipoProducto(rs.getString("t.tipr_descripcion"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }

        return listado;
    }
}
