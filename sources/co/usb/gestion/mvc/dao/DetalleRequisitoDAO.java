/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dto.DetalleRequisitoDTO;
import co.usb.gestion.mvc.dto.TipoRequisitoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class DetalleRequisitoDAO {

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarDetalleRequisito(Connection conexion, DetalleRequisitoDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO detalle (deta_cantidad, deta_tiempo, nifo_id, tipr_id, supr_id, reti_id, tiva_id, deta_grupoclase) ");
            cadSQL.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getCantidad(), ps);
            AsignaAtributoStatement.setString(2, datos.getTiempo(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdNivelFormacion(), ps);
            AsignaAtributoStatement.setString(4, datos.getIdTipoProducto(), ps);
            AsignaAtributoStatement.setString(5, datos.getIdSubtipoProducto(), ps);
            AsignaAtributoStatement.setString(6, datos.getIdRequisitoTipoInvestigador(), ps);
            AsignaAtributoStatement.setString(7, datos.getIdTipoValida(), ps);
            AsignaAtributoStatement.setString(8, datos.getAgrupadorClase(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    datos.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<DetalleRequisitoDTO> consultarDetalleRequisitos(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<DetalleRequisitoDTO> listado = null;
        DetalleRequisitoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT d.deta_id,d.reti_id,d.tipr_id,tp.tipr_descripcion,d.supr_id,su.supr_descripcion,d.nifo_id,ni.nifo_descripcion,d.tiva_id, ");
            cadSQL.append(" d.deta_tiempo,d.deta_cantidad,d.deta_grupoclase ");
            cadSQL.append(" FROM detalle d ");
            cadSQL.append(" LEFT JOIN tipo_producto tp ON d.tipr_id = tp.tipr_id ");
            cadSQL.append(" LEFT JOIN subtipo_producto su ON d.supr_id = su.supr_id  ");
            cadSQL.append(" LEFT JOIN nivel_formacion ni ON d.nifo_id = ni.nifo_id ");

            ps = conexion.prepareStatement(cadSQL.toString());
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new DetalleRequisitoDTO();
                datos.setId(rs.getString("deta_id"));
                datos.setIdRequisitoTipoInvestigador(rs.getString("reti_id"));
                datos.setIdTipoProducto(rs.getString("tipr_id"));
                datos.setDescripcionTipoProducto(rs.getString("tipr_descripcion"));
                datos.setIdSubtipoProducto(rs.getString("supr_id"));
                datos.setDescripcionSubtipoProducto(rs.getString("supr_descripcion"));
                datos.setIdNivelFormacion(rs.getString("nifo_id"));
                datos.setDescripcionNivelFormacion(rs.getString("nifo_descripcion"));
                datos.setIdTipoValida(rs.getString("tiva_id"));
                datos.setTiempo(rs.getString("deta_tiempo"));
                datos.setCantidad(rs.getString("deta_cantidad"));
                datos.setAgrupadorClase(rs.getString("deta_grupoclase"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param idRequisito
     * @return
     */
    public ArrayList<DetalleRequisitoDTO> consultarDetalleRequisitosPorIdRequisito(Connection conexion, String idRequisito) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<DetalleRequisitoDTO> listado = null;
        DetalleRequisitoDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT d.deta_id,d.reti_id,d.tipr_id,tp.tipr_descripcion,d.supr_id,su.supr_descripcion,d.nifo_id,ni.nifo_descripcion,d.tiva_id, ");
            cadSQL.append(" d.deta_tiempo,d.deta_cantidad,d.deta_grupoclase ");
            cadSQL.append(" FROM detalle d ");
            cadSQL.append(" LEFT JOIN tipo_producto tp ON d.tipr_id = tp.tipr_id ");
            cadSQL.append(" LEFT JOIN subtipo_producto su ON d.supr_id = su.supr_id  ");
            cadSQL.append(" LEFT JOIN nivel_formacion ni ON d.nifo_id = ni.nifo_id ");
            cadSQL.append(" WHERE reti_id=?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idRequisito, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();
            while (rs.next()) {
                datos = new DetalleRequisitoDTO();
                datos.setId(rs.getString("deta_id"));
                datos.setIdRequisitoTipoInvestigador(rs.getString("reti_id"));
                datos.setIdTipoProducto(rs.getString("tipr_id"));
                datos.setDescripcionTipoProducto(rs.getString("tipr_descripcion"));
                datos.setIdSubtipoProducto(rs.getString("supr_id"));
                datos.setDescripcionSubtipoProducto(rs.getString("supr_descripcion"));
                datos.setIdNivelFormacion(rs.getString("nifo_id"));
                datos.setDescripcionNivelFormacion(rs.getString("nifo_descripcion"));
                datos.setIdTipoValida(rs.getString("tiva_id"));
                datos.setTiempo(rs.getString("deta_tiempo"));
                datos.setCantidad(rs.getString("deta_cantidad"));
                datos.setAgrupadorClase(rs.getString("deta_grupoclase"));

                listado.add(datos);

            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

//    /**
//     *
//     * @param conexion
//     * @param datos
//     * @return
//     */
//    public boolean actualizarTipoRequisito(Connection conexion, TipoRequisitoDTO datos) {
//
//        PreparedStatement ps = null;
//        int nRows = 0;
//        StringBuilder cadSQL = null;
//        boolean registroExitoso = false;
//
//        try {
//
//            cadSQL = new StringBuilder();
//            cadSQL.append(" UPDATE tipo_requisito SET tire_descripcion = ?, tire_estado = ? WHERE tire_id = ? ");
//
//            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
//            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
//            AsignaAtributoStatement.setString(2, datos.getEstado(), ps);
//            AsignaAtributoStatement.setString(3, datos.getId(), ps);
//
//            nRows = ps.executeUpdate();
//            if (nRows > 0) {
//                registroExitoso = true;
//            }
//        } catch (SQLException se) {
//            LoggerMessage.getInstancia().loggerMessageException(se);
//            return false;
//        }
//        return registroExitoso;
//    }
}
