/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.constantes.Constantes;
import co.usb.gestion.mvc.dto.RequisitoTipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.TipoRequisitoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class RequisitoTipoInvestigadorDAO {

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarTipoRequisito(Connection conexion, RequisitoTipoInvestigadorDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO requisito_tipoinvestigador (tire_id, opci_id, suin_id) ");
            cadSQL.append(" VALUES (?, ?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getIdTipoRequisito(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdOpcion(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdsubTipoInvestigador(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    datos.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<RequisitoTipoInvestigadorDTO> consultarRequisitos(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<RequisitoTipoInvestigadorDTO> listado = null;
        RequisitoTipoInvestigadorDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT r.reti_id,r.tire_id,tr.tire_descripcion,r.opci_id,op.opci_descripcion,r.suin_id,su.suin_descripcion,su.suin_codigo ");
            cadSQL.append(" FROM requisito_tipoinvestigador r ");
            cadSQL.append(" INNER JOIN tipo_requisito tr ON r.tire_id = tr.tire_id ");
            cadSQL.append(" INNER JOIN opcion op ON r.opci_id = op.opci_id  ");
            cadSQL.append(" INNER JOIN subtipo_investigador su ON r.suin_id = su.suin_id WHERE r.reti_estado = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new RequisitoTipoInvestigadorDTO();
                datos.setId(rs.getString("reti_id"));
                datos.setIdTipoRequisito(rs.getString("tire_id"));
                datos.setDescripcionTipoRequisito(rs.getString("tire_descripcion"));
                datos.setIdOpcion(rs.getString("opci_id"));
                datos.setDescripcionOpcion(rs.getString("opci_descripcion"));
                datos.setIdsubTipoInvestigador(rs.getString("suin_id"));
                datos.setDescripcionSubTipoInvestigador(rs.getString("suin_descripcion"));
                datos.setCodigoSubTipoInvestigador(rs.getString("suin_codigo"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

    /**
     *
     * @param conexion
     * @param idSubtipo
     * @return
     */
    public ArrayList<RequisitoTipoInvestigadorDTO> consultarRequisitosPorSubtipoInvestiador(Connection conexion, String idSubtipo) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<RequisitoTipoInvestigadorDTO> listado = null;
        RequisitoTipoInvestigadorDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT r.reti_id,r.tire_id,tr.tire_descripcion,r.opci_id,op.opci_descripcion,r.suin_id,su.suin_descripcion,su.suin_codigo ");
            cadSQL.append(" FROM requisito_tipoinvestigador r ");
            cadSQL.append(" INNER JOIN tipo_requisito tr ON r.tire_id = tr.tire_id ");
            cadSQL.append(" INNER JOIN opcion op ON r.opci_id = op.opci_id  ");
            cadSQL.append(" INNER JOIN subtipo_investigador su ON r.suin_id = su.suin_id ");
            cadSQL.append(" WHERE r.suin_id = ? AND r.reti_estado = ?");
            cadSQL.append(" ORDER BY r.tire_id,r.opci_id ASC");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idSubtipo, ps);
            AsignaAtributoStatement.setString(2, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new RequisitoTipoInvestigadorDTO();
                datos.setId(rs.getString("reti_id"));
                datos.setIdTipoRequisito(rs.getString("tire_id"));
                datos.setDescripcionTipoRequisito(rs.getString("tire_descripcion"));
                datos.setIdOpcion(rs.getString("opci_id"));
                datos.setDescripcionOpcion(rs.getString("opci_descripcion"));
                datos.setIdsubTipoInvestigador(rs.getString("suin_id"));
                datos.setDescripcionSubTipoInvestigador(rs.getString("suin_descripcion"));
                datos.setCodigoSubTipoInvestigador(rs.getString("suin_codigo"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

//    /**
//     *
//     * @param conexion
//     * @param datos
//     * @return
//     */
//    public boolean actualizarTipoRequisito(Connection conexion, TipoRequisitoDTO datos) {
//
//        PreparedStatement ps = null;
//        int nRows = 0;
//        StringBuilder cadSQL = null;
//        boolean registroExitoso = false;
//
//        try {
//
//            cadSQL = new StringBuilder();
//            cadSQL.append(" UPDATE tipo_requisito SET tire_descripcion = ?, tire_estado = ? WHERE tire_id = ? ");
//
//            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
//            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
//            AsignaAtributoStatement.setString(2, datos.getEstado(), ps);
//            AsignaAtributoStatement.setString(3, datos.getId(), ps);
//
//            nRows = ps.executeUpdate();
//            if (nRows > 0) {
//                registroExitoso = true;
//            }
//        } catch (SQLException se) {
//            LoggerMessage.getInstancia().loggerMessageException(se);
//            return false;
//        }
//        return registroExitoso;
//    }
    /**
     *
     * @param conexion
     * @param documento
     * @return
     */
    public RequisitoTipoInvestigadorDTO validarRequisitoTipoInvestigador(Connection conexion, RequisitoTipoInvestigadorDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT reti_id ");
            cadSQL.append(" FROM requisito_tipoinvestigador ");
            cadSQL.append(" WHERE  tire_id = ? AND opci_id = ? AND suin_id = ? AND reti_estado = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, datos.getIdTipoRequisito(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdOpcion(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdsubTipoInvestigador(), ps);
            AsignaAtributoStatement.setString(4, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();

            if (rs.next()) {
                datos.setId(rs.getString("reti_id"));
            } else {
                datos = null;
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return datos;
    }
}
