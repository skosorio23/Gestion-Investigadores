/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dao;

import co.usb.gestion.common.util.AsignaAtributoStatement;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.constantes.Constantes;
import co.usb.gestion.mvc.dto.TipoProfesorDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class TipoProfesorDAO {

    /**
     *
     * @param conexion
     * @param datos
     * @return
     */
    public boolean registrarTipoProfesor(Connection conexion, TipoProfesorDTO datos) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registroExitoso = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO tipo_producto (tipr_descripcion, tipr_estado) ");
            cadSQL.append(" VALUES (?, ?) ");

            ps = conexion.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(2, datos.getEstado(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registroExitoso = true;
                    datos.setId(rs.getString(1));
                }
                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            LoggerMessage.getInstancia().loggerMessageException(se);
            return false;
        }
        return registroExitoso;
    }

    /**
     *
     * @param conexion
     * @return
     */
    public ArrayList<TipoProfesorDTO> listarTipoProfesor(Connection conexion) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<TipoProfesorDTO> listado = null;
        TipoProfesorDTO datos = null;
        StringBuilder cadSQL = null;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tipr_id, tipr_descripcion, tipr_estado FROM tipo_profesor WHERE tipr_estado = ?");

            ps = conexion.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, Constantes.ESTADO_ACTIVO, ps);
            rs = ps.executeQuery();
            listado = new ArrayList();

            while (rs.next()) {
                datos = new TipoProfesorDTO();
                datos.setId(rs.getString("tipr_id"));
                datos.setDescripcion(rs.getString("tipr_descripcion"));
                datos.setEstado(rs.getString("tipr_estado"));

                listado.add(datos);
            }

            ps.close();
            ps = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
                return null;
            }
        }
        return listado;
    }

}
