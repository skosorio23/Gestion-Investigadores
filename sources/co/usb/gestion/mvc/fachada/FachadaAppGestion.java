/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.fachada;

import co.usb.gestion.mvc.dto.ClaseProductoDTO;
import co.usb.gestion.mvc.dto.DetalleRequisitoDTO;
import co.usb.gestion.mvc.dto.FormacionAcademicalDTO;
import co.usb.gestion.mvc.dto.NivelFormacionDTO;
import co.usb.gestion.mvc.dto.OpcionRequisitoDTO;
import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.dto.PerfilProductoDTO;
import co.usb.gestion.mvc.dto.ProgramaAcademicoDTO;
import co.usb.gestion.mvc.dto.RespuestaNivelDTO;
import co.usb.gestion.mvc.dto.RequisitoTipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.SubTipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.SubtipoProductoDTO;
import co.usb.gestion.mvc.dto.TipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.TipoProductoDTO;
import co.usb.gestion.mvc.dto.TipoProfesorDTO;
import co.usb.gestion.mvc.dto.TipoRequisitoDTO;
import co.usb.gestion.mvc.dto.TipoUsuarioDTO;
import co.usb.gestion.mvc.dto.TipoValidaDTO;
import co.usb.gestion.mvc.dto.UsuarioDTO;
import co.usb.gestion.mvc.mediador.MediadorAppGestion;
import java.util.ArrayList;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

/**
 *
 * @author Sys. Hebert Medelo
 */
@RemoteProxy(name = "ajaxGestion", scope = ScriptScope.SESSION)
public class FachadaAppGestion {

    /**
     *
     */
    public FachadaAppGestion() {
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public boolean servicioActivo() {
        return true;
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoUsuarioDTO> listarTipoUsuario() {
        return MediadorAppGestion.getInstancia().listarTipoUsuario();
    }

    /**
     * @param idTipoInv
     * @return
     */
    @RemoteMethod
    public ArrayList<SubTipoInvestigadorDTO> listarSubTipoInvestigador(String idTipoInv) {
        return MediadorAppGestion.getInstancia().listarSubTipoInvestigador(idTipoInv);
    }

    /**
     * @param usuario
     * @param perfil
     * @return
     */
    @RemoteMethod
    public boolean registrarUsuario(UsuarioDTO usuario, PerfilDTO perfil) {
        return MediadorAppGestion.getInstancia().registrarUsuario(usuario, perfil);
    }

    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilDTO> consultarPerfilPorNombreDocumento(String condicion) {
        return MediadorAppGestion.getInstancia().consultarPerfilPorNombreDocumento(condicion);
    }

    /**
     *
     * @param datosUsuario
     * @return
     */
    @RemoteMethod
    public boolean actualizarEstadoUsuario(UsuarioDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarEstadoUsuario(datos);
    }

    /**
     *
     * @param documento
     * @return
     */
    public UsuarioDTO recuperarContrasenia(String documento) {
        return MediadorAppGestion.getInstancia().recuperarContrasenia(documento);
    }

    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilDTO> consultarGestionPerfilProducto(String condicion) {
        return MediadorAppGestion.getInstancia().consultarGestionPerfilProducto(condicion);
    }

    /**
     * @param idPerfil
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilProductoDTO> ConsultarPerfilProducto(String idPerfil) {
        return MediadorAppGestion.getInstancia().ConsultarPerfilProducto(idPerfil);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoInvestigadorDTO> listarTipoInvestigador() {
        return MediadorAppGestion.getInstancia().listarTipoInvestigador();
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<NivelFormacionDTO> listarNivelFormacion() {
        return MediadorAppGestion.getInstancia().listarNivelFormacion();
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilDTO> listarPerfilInvestigador() {
        return MediadorAppGestion.getInstancia().listarPerfilInvestigador();
    }

    /**
     * @param formacion
     * @return
     */
    @RemoteMethod
    public boolean registrarInfAcademica(FormacionAcademicalDTO formacion) {
        return MediadorAppGestion.getInstancia().registrarInfAcademica(formacion);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarFormacionAcad(FormacionAcademicalDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarFormacionAcad(datos);
    }

    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorInstitucionPrograma(String condicion) {
        return MediadorAppGestion.getInstancia().consultarFormacionPorInstitucionPrograma(condicion);
    }

    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorInstitucionProgramaOperacional(String condicion) {
        return MediadorAppGestion.getInstancia().consultarFormacionPorInstitucionProgramaOperacional(condicion);
    }

    /**
     * @param usuario
     * @param perfil
     * @return
     */
    @RemoteMethod
    public boolean actualizarPerfil(PerfilDTO perfil, UsuarioDTO usuario) {
        return MediadorAppGestion.getInstancia().actualizarPerfil(perfil, usuario);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilDTO> listarUsuarios() {
        return MediadorAppGestion.getInstancia().listarUsuarios();
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<SubtipoProductoDTO> listarSubtipoProducto() {
        return MediadorAppGestion.getInstancia().listarSubtipoProducto();
    }

    /**
     * @param Datos
     * @return
     */
    @RemoteMethod
    public boolean registrarPerfilProducto(PerfilProductoDTO Datos) {
        return MediadorAppGestion.getInstancia().registrarPerfilProducto(Datos);
    }

    @RemoteMethod
    public boolean actualizarPerfilProducto(PerfilProductoDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarPerfilProducto(datos);
    }

    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilProductoDTO> consultarSubtipo(String condicion) {
        return MediadorAppGestion.getInstancia().consultarSubtipo(condicion);
    }

    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilProductoDTO> consultarSubtipoOperacional(String condicion) {
        return MediadorAppGestion.getInstancia().consultarSubtipoOperacional(condicion);
    }

    /**
     * @param usuario
     * @param perfil
     * @return
     */
    @RemoteMethod
    public boolean registrarOpcionRequisito(OpcionRequisitoDTO datos) {
        return MediadorAppGestion.getInstancia().registrarOpcionRequisito(datos);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<OpcionRequisitoDTO> listarOpcionRequisito() {
        return MediadorAppGestion.getInstancia().listarOpcionRequisito();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarOpcionRequisito(OpcionRequisitoDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarOpcionRequisito(datos);
    }

    /**
     * @param usuario
     * @param perfil
     * @return
     */
    @RemoteMethod
    public boolean registrarTipoRequisito(TipoRequisitoDTO datos) {
        return MediadorAppGestion.getInstancia().registrarTipoRequisito(datos);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoRequisitoDTO> listarTipoRequisito() {
        return MediadorAppGestion.getInstancia().listarTipoRequisito();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarTipoRequisito(TipoRequisitoDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarTipoRequisito(datos);
    }

    /**
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarTipoUsuario(TipoUsuarioDTO datos) {
        return MediadorAppGestion.getInstancia().registrarTipoUsuario(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarTipoUsuario(TipoUsuarioDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarTipoUsuario(datos);
    }

    /**
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarTipoInvestigador(TipoInvestigadorDTO datos) {
        return MediadorAppGestion.getInstancia().registrarTipoInvestigador(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarTipoInvestigador(TipoInvestigadorDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarTipoInvestigador(datos);
    }

    /**
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarSubtipoInvestigador(SubTipoInvestigadorDTO datos) {
        return MediadorAppGestion.getInstancia().registrarSubtipoInvestigador(datos);
    }

    /**
     * @return
     */
    @RemoteMethod
    public ArrayList<SubTipoInvestigadorDTO> listarTodosSubTipoInvestigador() {
        return MediadorAppGestion.getInstancia().listarTodosSubTipoInvestigador();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarSubipoInvestigador(SubTipoInvestigadorDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarSubipoInvestigador(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarTipoProducto(TipoProductoDTO datos) {
        return MediadorAppGestion.getInstancia().registrarTipoProducto(datos);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoProductoDTO> listarTipoProducto() {
        return MediadorAppGestion.getInstancia().listarTipoProducto();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarTipoProducto(TipoProductoDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarTipoProducto(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarSubtipoProducto(SubtipoProductoDTO datos) {
        return MediadorAppGestion.getInstancia().registrarSubtipoProducto(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarSubipoProducto(SubtipoProductoDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarSubipoProducto(datos);
    }

    /**
     * @param idProducto
     * @return
     */
    @RemoteMethod
    public ArrayList<SubtipoProductoDTO> listarSubtipoProductoPorIdProducto(String idProducto) {
        return MediadorAppGestion.getInstancia().listarSubtipoProductoPorIdProducto(idProducto);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarClaseProducto(ClaseProductoDTO datos) {
        return MediadorAppGestion.getInstancia().registrarClaseProducto(datos);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<ClaseProductoDTO> listarClaseProducto() {
        return MediadorAppGestion.getInstancia().listarClaseProducto();
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<ClaseProductoDTO> listarClaseProductoSub(String idTipoP) {
        return MediadorAppGestion.getInstancia().listarClaseProductoSub(idTipoP);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarClaseProducto(ClaseProductoDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarClaseProducto(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarNivelFormacion(NivelFormacionDTO datos) {
        return MediadorAppGestion.getInstancia().registrarNivelFormacion(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarNivelFormacion(NivelFormacionDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarNivelFormacion(datos);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoValidaDTO> listarTipoValida() {
        return MediadorAppGestion.getInstancia().listarTipoValida();
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarTipoValida(TipoValidaDTO datos) {
        return MediadorAppGestion.getInstancia().registrarTipoValida(datos);
    }

    /**
     *
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean actualizarTipoValida(TipoValidaDTO datos) {
        return MediadorAppGestion.getInstancia().actualizarTipoValida(datos);
    }

    /**
     *
     * @param datos1
     * @param datos
     * @return
     */
    @RemoteMethod
    public boolean registrarDetalleRequisito(RequisitoTipoInvestigadorDTO datos1, DetalleRequisitoDTO datos) {
        return MediadorAppGestion.getInstancia().registrarDetalleRequisito(datos1, datos);
    }

    /**
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilProductoDTO> listadoPerfilProducto() {
        return MediadorAppGestion.getInstancia().listadoPerfilProducto();
    }

    /**
     *
     * @param idPerfil
     * @return
     */
    @RemoteMethod
    public PerfilDTO consultarObservacion(String idPerfil) {
        return MediadorAppGestion.getInstancia().consultarObservacion(idPerfil);
    }

    /**
     * @param condicion
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilProductoDTO> consultarPerfilProductoXCondicion(String condicion) {
        return MediadorAppGestion.getInstancia().consultarPerfilProductoXCondicion(condicion);
    }

    /**
     *
     * @param idPerfil
     * @return
     */
    @RemoteMethod
    public RespuestaNivelDTO verNivelInvestigador(String idPerfil) {
        return MediadorAppGestion.getInstancia().verNivelInvestigador(idPerfil);
    }

    /*SIMULACION*/
    /**
     * @param formacion
     * @return
     */
    @RemoteMethod
    public boolean registrarInfAcademicaSimulacion(FormacionAcademicalDTO formacion) {
        return MediadorAppGestion.getInstancia().registrarInfAcademicaSimulacion(formacion);
    }

    /**
     * @param Datos
     * @return
     */
    @RemoteMethod
    public boolean registrarPerfilProductoSimulacion(PerfilProductoDTO Datos) {
        return MediadorAppGestion.getInstancia().registrarPerfilProductoSimulacion(Datos);
    }

    /**
     *
     * @param idPerfil
     * @return
     */
    @RemoteMethod
    public RespuestaNivelDTO verNivelInvestigadorSimulador(String idPerfil) {
        return MediadorAppGestion.getInstancia().verNivelInvestigadorSimulador(idPerfil);
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<ProgramaAcademicoDTO> listarProgramaAcademico() {
        return MediadorAppGestion.getInstancia().listarProgramaAcademico();
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<TipoProfesorDTO> listarTipoProfesor() {
        return MediadorAppGestion.getInstancia().listarTipoProfesor();
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public ArrayList<PerfilDTO> listarUsuariosInvestigadores() {
        return MediadorAppGestion.getInstancia().listarUsuariosInvestigadores();
    }

    @RemoteMethod
    public boolean limpiarsimulador() {
        return MediadorAppGestion.getInstancia().limpiarsimulador();
    }
}
