/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dto;

import co.usb.gestion.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class DetalleRequisitoDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String idRequisitoTipoInvestigador = Generales.EMPTYSTRING;
    String idTipoProducto = Generales.EMPTYSTRING;
    String descripcionTipoProducto = Generales.EMPTYSTRING;
    String idSubtipoProducto = Generales.EMPTYSTRING;
    String descripcionSubtipoProducto = Generales.EMPTYSTRING;
    String idNivelFormacion = Generales.EMPTYSTRING;
    String descripcionNivelFormacion = Generales.EMPTYSTRING;
    String idTipoValida = Generales.EMPTYSTRING;
    String descripcionTipoValida = Generales.EMPTYSTRING;
    String tiempo = Generales.EMPTYSTRING;
    String cantidad = Generales.EMPTYSTRING;
    String agrupadorClase = Generales.EMPTYSTRING;
    String descripcionRequisito = Generales.EMPTYSTRING;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdRequisitoTipoInvestigador() {
        return idRequisitoTipoInvestigador;
    }

    public void setIdRequisitoTipoInvestigador(String idRequisitoTipoInvestigador) {
        this.idRequisitoTipoInvestigador = idRequisitoTipoInvestigador;
    }

    public String getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(String idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public String getDescripcionTipoProducto() {
        return descripcionTipoProducto;
    }

    public void setDescripcionTipoProducto(String descripcionTipoProducto) {
        this.descripcionTipoProducto = descripcionTipoProducto;
    }

    public String getIdSubtipoProducto() {
        return idSubtipoProducto;
    }

    public void setIdSubtipoProducto(String idSubtipoProducto) {
        this.idSubtipoProducto = idSubtipoProducto;
    }

    public String getDescripcionSubtipoProducto() {
        return descripcionSubtipoProducto;
    }

    public void setDescripcionSubtipoProducto(String descripcionSubtipoProducto) {
        this.descripcionSubtipoProducto = descripcionSubtipoProducto;
    }

    public String getIdNivelFormacion() {
        return idNivelFormacion;
    }

    public void setIdNivelFormacion(String idNivelFormacion) {
        this.idNivelFormacion = idNivelFormacion;
    }

    public String getDescripcionNivelFormacion() {
        return descripcionNivelFormacion;
    }

    public void setDescripcionNivelFormacion(String descripcionNivelFormacion) {
        this.descripcionNivelFormacion = descripcionNivelFormacion;
    }

    public String getIdTipoValida() {
        return idTipoValida;
    }

    public void setIdTipoValida(String idTipoValida) {
        this.idTipoValida = idTipoValida;
    }

    public String getDescripcionTipoValida() {
        return descripcionTipoValida;
    }

    public void setDescripcionTipoValida(String descripcionTipoValida) {
        this.descripcionTipoValida = descripcionTipoValida;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getAgrupadorClase() {
        return agrupadorClase;
    }

    public void setAgrupadorClase(String agrupadorClase) {
        this.agrupadorClase = agrupadorClase;
    }

    public String getDescripcionRequisito() {
        return descripcionRequisito;
    }

    public void setDescripcionRequisito(String descripcionRequisito) {
        this.descripcionRequisito = descripcionRequisito;
    }

    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
