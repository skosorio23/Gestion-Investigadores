/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.mediador;

import co.usb.gestion.common.connection.ContextDataResourceNames;
import co.usb.gestion.common.connection.DataBaseConnection;
import co.usb.gestion.common.util.Generales;
import co.usb.gestion.common.util.LoggerMessage;
import co.usb.gestion.mvc.dao.ClaseProductoDAO;
import co.usb.gestion.mvc.dao.DetalleRequisitoDAO;
import co.usb.gestion.mvc.dao.FormacionAcademicalDAO;
import co.usb.gestion.mvc.dao.NivelFormacionDAO;
import co.usb.gestion.mvc.dao.OpcionRequisitoDAO;
import co.usb.gestion.mvc.dao.PerfilDAO;
import co.usb.gestion.mvc.dao.PerfilProductoDAO;
import co.usb.gestion.mvc.dao.ProgramaAcademicoDAO;
import co.usb.gestion.mvc.dao.RequisitoTipoInvestigadorDAO;
import co.usb.gestion.mvc.dao.SubTipoInvestigadorDAO;
import co.usb.gestion.mvc.dao.SubtipoProductoDAO;
import co.usb.gestion.mvc.dao.TipoInvestigadorDAO;
import co.usb.gestion.mvc.dao.TipoProductoDAO;
import co.usb.gestion.mvc.dao.TipoProfesorDAO;
import co.usb.gestion.mvc.dao.TipoRequisitoDAO;
import co.usb.gestion.mvc.dao.TipoUsuarioDAO;
import co.usb.gestion.mvc.dao.TipoValidaDAO;
import co.usb.gestion.mvc.dao.UsuarioDAO;
import co.usb.gestion.mvc.dto.ClaseProductoDTO;
import co.usb.gestion.mvc.dto.DetalleRequisitoDTO;
import co.usb.gestion.mvc.dto.FormacionAcademicalDTO;
import co.usb.gestion.mvc.dto.NivelFormacionDTO;
import co.usb.gestion.mvc.dto.OpcionRequisitoDTO;
import co.usb.gestion.mvc.dto.UsuarioDTO;
import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.dto.PerfilProductoDTO;
import co.usb.gestion.mvc.dto.ProgramaAcademicoDTO;
import co.usb.gestion.mvc.dto.RequisitoTipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.RespuestaNivelDTO;
import co.usb.gestion.mvc.dto.SubTipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.SubtipoProductoDTO;
import co.usb.gestion.mvc.dto.TipoInvestigadorDTO;
import co.usb.gestion.mvc.dto.TipoProductoDTO;
import co.usb.gestion.mvc.dto.TipoProfesorDTO;
import co.usb.gestion.mvc.dto.TipoRequisitoDTO;
import co.usb.gestion.mvc.dto.TipoUsuarioDTO;
import co.usb.gestion.mvc.dto.TipoValidaDTO;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.http.HttpSession;
import org.directwebremoting.WebContextFactory;

/**
 *
 * @author Sys. Hebert Medelo
 */
public class MediadorAppGestion {

    /**
     *
     */
    private final static MediadorAppGestion instancia = null;
    private final LoggerMessage logMsg;

    /**
     *
     */
    public MediadorAppGestion() {
        logMsg = LoggerMessage.getInstancia();
    }

    /**
     *
     * @return
     */
    public static synchronized MediadorAppGestion getInstancia() {
        return instancia == null ? new MediadorAppGestion() : instancia;
    }

    public void pasarGarbageCollector() {
        Runtime garbage = Runtime.getRuntime();
        garbage.gc();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     *
     */
    /**
     * @param idTipoInv
     * @return
     */
    public ArrayList<SubTipoInvestigadorDTO> listarSubTipoInvestigador(String idTipoInv) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new SubTipoInvestigadorDAO().listarSubTipoInvestigador(conexion, idTipoInv);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<TipoUsuarioDTO> listarTipoUsuario() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new TipoUsuarioDAO().listarTipoUsuario(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param documento
     * @return
     */
    public boolean validarDocumento(String documento) {
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean usuarioValido = false;
        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            usuarioValido = new PerfilDAO().validarDocumento(conexion, documento);

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return usuarioValido;
    }

    /**
     *
     * @param datosUsuario
     * @param perfil
     * @return
     */
    public boolean registrarUsuario(UsuarioDTO datosUsuario, PerfilDTO perfil) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            conexion.setAutoCommit(false);

            if (!new UsuarioDAO().registrarUsuario(conexion, datosUsuario)) {
                throw new Exception("Error al registrar usuario.");
            }

            perfil.setIdUsuario(datosUsuario.getId());

            if (!new PerfilDAO().registrarPerfil(conexion, perfil)) {
                throw new Exception("Error al registrar perfil.");
            }
            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (registroExitoso) {
                    conexion.commit();
                } else {
                    conexion.rollback();
                }
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param user
     * @return
     */
    public boolean validarUsuario(String user) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean validarUsuario = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            conexion.setAutoCommit(false);

            validarUsuario = new UsuarioDAO().validarUsuario(conexion, user);

            if (validarUsuario != false) {
                conexion.commit();
                validarUsuario = true;
            } else {
                conexion.rollback();
            }
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return validarUsuario;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<PerfilDTO> consultarPerfilPorNombreDocumento(String condicion) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilDAO().consultarPerfilPorNombreDocumento(conexion, condicion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarEstadoUsuario(UsuarioDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new UsuarioDAO().actualizarEstadoUsuario(conexion, datos)) {
                throw new Exception("Error al cambiar el estado.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param documento
     * @return
     */
    public UsuarioDTO recuperarContrasenia(String documento) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        UsuarioDTO datos = null;
        String nuevaContrasenia = Generales.EMPTYSTRING;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            datos = new PerfilDAO().recuperarContrasenia(conexion, documento);

            if (datos != null) {

                String usauId = datos.getId();
                nuevaContrasenia = UUID.randomUUID().toString().substring(0, 6);

                if (!new UsuarioDAO().generarContraseña(conexion, usauId, nuevaContrasenia)) {
                    throw new Exception("no se actualiza la coontrasaña");
                }
                datos.setClave(nuevaContrasenia);
            }

            conexion.close();
            conexion = null;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return datos;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<PerfilDTO> consultarGestionPerfilProducto(String condicion) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<PerfilDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilDAO().consultarGestionPerfilProducto(conexion, condicion);

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<TipoInvestigadorDTO> listarTipoInvestigador() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoInvestigadorDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new TipoInvestigadorDAO().listarTipoInvestigador(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     * @param idPerfil
     * @return
     */
    public ArrayList<PerfilProductoDTO> ConsultarPerfilProducto(String idPerfil) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<PerfilProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilProductoDAO().ConsultarPerfilProducto(conexion, idPerfil);

            for (int i = 0; i < listado.size(); i++) {
                System.out.println("-------------- " + listado.get(i).toStringJson());
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<NivelFormacionDTO> listarNivelFormacion() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<NivelFormacionDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new NivelFormacionDAO().listarNivelFormacion(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<PerfilDTO> listarPerfilInvestigador() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<PerfilDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilDAO().listarPerfilInvestigador(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param formacion
     * @return
     */
    public boolean registrarInfAcademica(FormacionAcademicalDTO formacion) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean registroExitoso = true;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            if ((formacion.getPerfil() == null) || ("".equals(formacion.getPerfil()))) {
                // cadena  está vacía                   
                HttpSession session = WebContextFactory.get().getSession();
                PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
                formacion.setPerfilID(usuarioLogueado.getIdPerfil());
            } else {
                formacion.setPerfilID(formacion.getPerfil());
            }
            /*HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            formacion.setPerfilID(usuarioLogueado.getIdPerfil());*/
            if (!new FormacionAcademicalDAO().registrarFormacion(conexion, formacion)) {
                throw new Exception("Error al registrar formación academica.");

            }
            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param datos
     * @param user
     * @return
     */
    public boolean actualizarPerfil(PerfilDTO datos, UsuarioDTO user) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new PerfilDAO().actualizarPerfil(conexion, datos)) {
                throw new Exception("Error al cambiar el estado.");
            }

            if (!new UsuarioDAO().actualizarUsuario(conexion, user)) {
                throw new Exception("Error al cambiar el estado.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean actualizarFormacionAcad(FormacionAcademicalDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            if ((datos.getPerfil() == null) || ("".equals(datos.getPerfil()))) {
                // cadena  está vacía             
                datos.setPerfilID(usuarioLogueado.getIdPerfil());
            } else {
                datos.setPerfilID(datos.getPerfil());
            }
            if (!new FormacionAcademicalDAO().actualizarFormacionAcad(conexion, datos)) {
                throw new Exception("Error al actualizar formación academica.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorInstitucionPrograma(String condicion) {
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new FormacionAcademicalDAO().consultarFormacionPorInstitucionPrograma(conexion, condicion, usuarioLogueado.getIdPerfil());

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<FormacionAcademicalDTO> consultarFormacionPorInstitucionProgramaOperacional(String condicion) {
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new FormacionAcademicalDAO().consultarFormacionPorInstitucionProgramaOperacional(conexion, condicion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @return
     */
    public ArrayList<PerfilDTO> listarUsuarios() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilDAO().listarUsuarios(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param Datos
     * @return
     */
    public boolean registrarPerfilProducto(PerfilProductoDTO Datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean registroExitoso = true;

        try {
            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            if ((Datos.getIdPerfil() == null) || ("".equals(Datos.getIdPerfil()))) {
                // cadena está vacía                
                Datos.setIdPerfil(usuarioLogueado.getIdPerfil());
            }
            /*conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            Datos.setIdPerfil(usuarioLogueado.getIdPerfil());*/
            if (!new PerfilProductoDAO().registrarPerfilProducto(conexion, Datos)) {
                throw new Exception("Error al registrar perfil producto.");

            }
            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    public boolean actualizarPerfilProducto(PerfilProductoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            if ((datos.getIdPerfil() == null) || ("".equals(datos.getIdPerfil()))) {
                // cadena está vacía                
                datos.setIdPerfil(usuarioLogueado.getIdPerfil());
            }
            if (!new PerfilProductoDAO().actualizarPerfilProducto(conexion, datos)) {
                throw new Exception("Error al actualizar perfil producto.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<PerfilProductoDTO> consultarSubtipo(String condicion) {
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            listado = new PerfilProductoDAO().consultarSubtipo(conexion, condicion, usuarioLogueado.getIdPerfil());
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<PerfilProductoDTO> consultarSubtipoOperacional(String condicion) {
        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            listado = new PerfilProductoDAO().consultarSubtipoOperacional(conexion, condicion);
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<SubtipoProductoDTO> listarSubtipoProducto() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<SubtipoProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new SubtipoProductoDAO().listarSubtipoProducto(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarOpcionRequisito(OpcionRequisitoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new OpcionRequisitoDAO().registrarOpcionRequisito(conexion, datos)) {
                throw new Exception("Error al registrar opcion.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @return
     */
    public ArrayList<OpcionRequisitoDTO> listarOpcionRequisito() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<OpcionRequisitoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new OpcionRequisitoDAO().listarOpcionRequisito(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarOpcionRequisito(OpcionRequisitoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new OpcionRequisitoDAO().actualizarOpcionRequisito(conexion, datos)) {
                throw new Exception("Error al cambiar el estado.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarTipoRequisito(TipoRequisitoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoRequisitoDAO().registrarTipoRequisito(conexion, datos)) {
                throw new Exception("Error al registrar opcion.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @return
     */
    public ArrayList<TipoRequisitoDTO> listarTipoRequisito() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoRequisitoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new TipoRequisitoDAO().listarTipoRequisito(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarTipoRequisito(TipoRequisitoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoRequisitoDAO().actualizarTipoRequisito(conexion, datos)) {
                throw new Exception("Error al cambiar el estado.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarTipoUsuario(TipoUsuarioDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoUsuarioDAO().registrarTipoUsuario(conexion, datos)) {
                throw new Exception("Error al registrar tipo de usurio.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarTipoUsuario(TipoUsuarioDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoUsuarioDAO().actualizarTipoUsuario(conexion, datos)) {
                throw new Exception("Error al cambiar el estado.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarTipoInvestigador(TipoInvestigadorDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoInvestigadorDAO().registrarTipoInvestigador(conexion, datos)) {
                throw new Exception("Error al registrar tipo de investigador.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarTipoInvestigador(TipoInvestigadorDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoInvestigadorDAO().actualizarTipoInvestigador(conexion, datos)) {
                throw new Exception("Error al actualizar.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarSubtipoInvestigador(SubTipoInvestigadorDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new SubTipoInvestigadorDAO().registrarSubtipoInvestigador(conexion, datos)) {
                throw new Exception("Error al registrar subtipo de investigador.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @return
     */
    public ArrayList<SubTipoInvestigadorDTO> listarTodosSubTipoInvestigador() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new SubTipoInvestigadorDAO().listarTodosSubTipoInvestigador(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarSubipoInvestigador(SubTipoInvestigadorDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new SubTipoInvestigadorDAO().actualizarSubipoInvestigador(conexion, datos)) {
                throw new Exception("Error al actualizar.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarTipoProducto(TipoProductoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoProductoDAO().registrarTipoProducto(conexion, datos)) {
                throw new Exception("Error al registrar tipo de investigador.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @return
     */
    public ArrayList<TipoProductoDTO> listarTipoProducto() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new TipoProductoDAO().listarTipoProducto(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarTipoProducto(TipoProductoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoProductoDAO().actualizarTipoProducto(conexion, datos)) {
                throw new Exception("Error al actualizar.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarSubtipoProducto(SubtipoProductoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new SubtipoProductoDAO().registrarSubtipoProducto(conexion, datos)) {
                throw new Exception("Error al registrar tipo de investigador.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarSubipoProducto(SubtipoProductoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new SubtipoProductoDAO().actualizarSubipoProducto(conexion, datos)) {
                throw new Exception("Error al actualizar.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param idProducto
     * @return
     */
    public ArrayList<SubtipoProductoDTO> listarSubtipoProductoPorIdProducto(String idProducto) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<SubtipoProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new SubtipoProductoDAO().listarSubtipoProductoPorIdProducto(conexion, idProducto);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarClaseProducto(ClaseProductoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new ClaseProductoDAO().registrarClaseProducto(conexion, datos)) {
                throw new Exception("Error al registrar tipo de investigador.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @return
     */
    public ArrayList<ClaseProductoDTO> listarClaseProducto() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<ClaseProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new ClaseProductoDAO().listarClaseProducto(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @return
     */
    public ArrayList<ClaseProductoDTO> listarClaseProductoSub(String idTipoP) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<ClaseProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new ClaseProductoDAO().listarClaseProductoSub(conexion, idTipoP);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarClaseProducto(ClaseProductoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new ClaseProductoDAO().actualizarClaseProducto(conexion, datos)) {
                throw new Exception("Error al actualizar.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarNivelFormacion(NivelFormacionDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new NivelFormacionDAO().registrarNivelFormacion(conexion, datos)) {
                throw new Exception("Error al registrar tipo de investigador.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarNivelFormacion(NivelFormacionDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new NivelFormacionDAO().actualizarNivelFormacion(conexion, datos)) {
                throw new Exception("Error al actualizar.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @return
     */
    public ArrayList<TipoValidaDTO> listarTipoValida() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoValidaDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new TipoValidaDAO().listarTipoValida(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarTipoValida(TipoValidaDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoValidaDAO().registrarTipoValida(conexion, datos)) {
                throw new Exception("Error al registrar tipo de investigador.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @param datos
     * @return
     */
    public boolean actualizarTipoValida(TipoValidaDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            if (!new TipoValidaDAO().actualizarTipoValida(conexion, datos)) {
                throw new Exception("Error al actualizar.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param datos
     * @return
     */
    public boolean registrarDetalleRequisito(RequisitoTipoInvestigadorDTO datos1, DetalleRequisitoDTO datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = true;
        RequisitoTipoInvestigadorDTO validar = null;

        try {
            //HttpSession session = WebContextFactory.get().getSession();
            //PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            //datosUsuario.setRegistradopor(usuarioLogueado.getUsuario());

            System.out.println("datos1" + datos1.toStringJson());
            System.out.println("datos" + datos.toStringJson());

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            conexion.setAutoCommit(false);

            validar = new RequisitoTipoInvestigadorDAO().validarRequisitoTipoInvestigador(conexion, datos1);

            if (validar == null) {
                if (!new RequisitoTipoInvestigadorDAO().registrarTipoRequisito(conexion, datos1)) {
                    throw new Exception("Error al registrar tipo de investigador.");
                }
                datos.setIdRequisitoTipoInvestigador(datos1.getId());
            } else {
                System.out.println("validarRequisitoTipoDetalle" + validar.toStringJson());
                datos.setIdRequisitoTipoInvestigador(validar.getId());
            }

            if (!new DetalleRequisitoDAO().registrarDetalleRequisito(conexion, datos)) {
                throw new Exception("Error al registrar tipo de investigador.");
            }

            registroExitoso = true;
        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (registroExitoso) {
                    conexion.commit();
                } else {
                    conexion.rollback();
                }
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     * @return
     */
    public ArrayList<PerfilProductoDTO> listadoPerfilProducto() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<PerfilProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilProductoDAO().listadoPerfilProducto(conexion);

            for (int i = 0; i < listado.size(); i++) {
                System.out.println("-------------- " + listado.get(i).toStringJson());
            }
            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     *
     * @param idPerfil
     * @return
     */
    public PerfilDTO consultarObservacion(String idPerfil) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        PerfilDTO datos = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            datos = new PerfilDAO().consultarObservacion(conexion, idPerfil);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }

            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return datos;
    }

    /**
     * @param condicion
     * @return
     */
    public ArrayList<PerfilProductoDTO> consultarPerfilProductoXCondicion(String condicion) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<PerfilProductoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilProductoDAO().consultarPerfilProductoXCondicion(conexion, condicion);

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    /**
     *
     * @param idPerfil
     * @return
     */
    public RespuestaNivelDTO verNivelInvestigador(String idPerfil) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;
        ArrayList<RequisitoTipoInvestigadorDTO> listadoRequisitos = null;
        ArrayList<DetalleRequisitoDTO> listadoDetalle = null;
        ArrayList<FormacionAcademicalDTO> datosFormacion = null;
        ArrayList<DetalleRequisitoDTO> listadoDetalleFaltante = null;
        ArrayList<DetalleRequisitoDTO> listadoDetalleTemporal = new ArrayList<DetalleRequisitoDTO>();
        DetalleRequisitoDTO datosFaltante = null;
        RespuestaNivelDTO respuesta = null;
        int requisitoContador = 1;
        boolean requisitoAprobado = false;
        int productos = 0;
        int productosAcumulados = 0;
        int subProductos = 0;
        int subProductosAcumulado = 0;
        String nivelInvestigador = "";

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            // consultar nivel de formacion del perfil
            System.out.println("id perfil " + idPerfil);
            datosFormacion = new FormacionAcademicalDAO().consultarFormacionPorIdPerfil(conexion, idPerfil);
            System.out.println("datos formacion " + datosFormacion);

            // consultar requisitos de senior
            listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "2");

            for (int i = 0; i < listadoRequisitos.size(); i++) {

                System.out.println(" TIPO DE REQUISITO  " + listadoRequisitos.get(i).getIdTipoRequisito());

                if (Integer.parseInt(listadoRequisitos.get(i).getIdTipoRequisito()) == (requisitoContador)) {

                    requisitoAprobado = false;

                    listadoDetalle = new DetalleRequisitoDAO().consultarDetalleRequisitosPorIdRequisito(conexion, listadoRequisitos.get(i).getId());

                    // recorrer el listado //
                    for (int j = 0; j < listadoDetalle.size(); j++) {

                        System.out.println(" detalle del requisito>>>>> " + listadoDetalle.get(j).toStringJson());

                        if (listadoDetalle.get(j).getIdTipoValida().equals("1")) {

                            System.out.println("tipon de validacion por formacion ");
                            // validar por formacion academica
                            if (datosFormacion != null) {
                                System.out.println(" datos de formacion diferente de null");
                                for (int k = 0; k < datosFormacion.size(); k++) {
                                    if (listadoDetalle.get(j).getIdNivelFormacion().equals(datosFormacion.get(k).getNivelID())) {
                                        System.out.println("cumplee la formacion");
                                        requisitoAprobado = true;
                                        break;
                                    }
                                    System.out.println("no cumple ");
                                }
                            }

                        } else if (listadoDetalle.get(j).getIdTipoValida().equals("2")) {
                            if (listadoRequisitos.get(i).getIdTipoRequisito().equals("3")) {
                                // consultar sub-productos del requisito 3
                                subProductos = new PerfilProductoDAO().consultarSubtipoPorId(conexion, idPerfil, listadoDetalle.get(j).getIdSubtipoProducto());
                                if (subProductos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;
                                }
//                            else if (subProductosAcumulado >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
//                                requisitoAprobado = true;
//
//                            }
                            } else {

                                // consultar productos dependiento el detalle 
                                productos = new PerfilProductoDAO().consultarProductosPorTipoProductoYClase(conexion, idPerfil, listadoDetalle.get(j).getIdTipoProducto(), listadoDetalle.get(j).getAgrupadorClase());
                                productosAcumulados = productosAcumulados + productos;
                                if (productos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;

                                } else if (productosAcumulados >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;

                                }
                                // si requisito aprobado = true ; break;
                                // no dar vuelta ,mantener array
                            }
                        }
                        if (requisitoAprobado == true) {
                            requisitoContador++;
//                            requisitoAprobado = false;
                            break;
                        }

                    }

                } else {
                    if (requisitoAprobado == false) {
                        break;
                    }

                }
            }
            if (requisitoAprobado == true) {
                nivelInvestigador = "2";
            }

            System.out.println("reqiosito ´probado ==== " + requisitoAprobado);
            System.out.println(" nivel de investigador ==== " + nivelInvestigador);

            if (requisitoAprobado == false && nivelInvestigador == "") {

                // consultar requisitos de asociado
                listadoRequisitos = null;
                requisitoAprobado = false;
                requisitoContador = 1;
                productos = 0;
                productosAcumulados = 0;
                subProductos = 0;
                subProductosAcumulado = 0;
                listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "3");

                for (int i = 0; i < listadoRequisitos.size(); i++) {

                    if (Integer.parseInt(listadoRequisitos.get(i).getIdTipoRequisito()) == (requisitoContador)) {

                        requisitoAprobado = false;

                        listadoDetalle = new DetalleRequisitoDAO().consultarDetalleRequisitosPorIdRequisito(conexion, listadoRequisitos.get(i).getId());

                        // recorrer el listado //
                        for (int j = 0; j < listadoDetalle.size(); j++) {

                            if (listadoDetalle.get(j).getIdTipoValida().equals("1")) {

                                // validar por formacion academica
                                if (datosFormacion != null) {
                                    for (int k = 0; k < datosFormacion.size(); k++) {
                                        if (listadoDetalle.get(j).getIdNivelFormacion().equals(datosFormacion.get(k).getNivelID())) {
                                            requisitoAprobado = true;
                                            break;
                                        }
                                    }
                                }

                            } else if (listadoDetalle.get(j).getIdTipoValida().equals("2")) {

                                if (listadoRequisitos.get(i).getIdTipoRequisito().equals("3")) {
                                    // consultar sub-productos del requisito 3
                                    subProductos = new PerfilProductoDAO().consultarSubtipoPorId(conexion, idPerfil, listadoDetalle.get(j).getIdSubtipoProducto());
                                    if (subProductos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;
                                    } else if (subProductosAcumulado >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;
                                    }
                                } else {

                                    // consultar productos dependiento el detalle 
                                    productos = new PerfilProductoDAO().consultarProductosPorTipoProductoYClase(conexion, idPerfil, listadoDetalle.get(j).getIdTipoProducto(), listadoDetalle.get(j).getAgrupadorClase());
                                    productosAcumulados = productosAcumulados + productos;
                                    if (productos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    } else if (productosAcumulados >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    }
                                    // si requisito aprobado = true ; break;
                                    // no dar vuelta ,mantener array
                                }
                            }

                            if (requisitoAprobado == true) {
                                requisitoContador++;
//                            requisitoAprobado = false;
                                break;
                            }

                        }

                    } else {
                        if (requisitoAprobado == false) {
                            break;
                        }

                    }

                }

                if (requisitoAprobado == true) {
                    nivelInvestigador = "3";
                }

            }

            if (requisitoAprobado == false) {

                // consultar requisitos de asociado
                listadoRequisitos = null;
                requisitoAprobado = false;
                requisitoContador = 1;
                productos = 0;
                productosAcumulados = 0;
                subProductos = 0;
                subProductosAcumulado = 0;
                listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "4");

                for (int i = 0; i < listadoRequisitos.size(); i++) {

                    if (Integer.parseInt(listadoRequisitos.get(i).getIdTipoRequisito()) == (requisitoContador)) {

                        requisitoAprobado = false;

                        listadoDetalle = new DetalleRequisitoDAO().consultarDetalleRequisitosPorIdRequisito(conexion, listadoRequisitos.get(i).getId());

                        // recorrer el listado //
                        for (int j = 0; j < listadoDetalle.size(); j++) {

                            if (listadoDetalle.get(j).getIdTipoValida().equals("1")) {

                                // validar por formacion academica
                                if (datosFormacion != null) {
                                    for (int k = 0; k < datosFormacion.size(); k++) {
                                        if (listadoDetalle.get(j).getIdNivelFormacion().equals(datosFormacion.get(k).getNivelID())) {
                                            requisitoAprobado = true;
                                            break;
                                        }
                                    }
                                }

                            } else if (listadoDetalle.get(j).getIdTipoValida().equals("2")) {
                                if (listadoRequisitos.get(i).getIdTipoRequisito().equals("3")) {
                                    // consultar sub-productos del requisito 3
                                    subProductos = new PerfilProductoDAO().consultarSubtipoPorId(conexion, idPerfil, listadoDetalle.get(j).getIdSubtipoProducto());
                                    if (subProductos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;
                                    } else if (subProductosAcumulado >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    }

                                } else {

                                    // consultar productos dependiento el detalle 
                                    productos = new PerfilProductoDAO().consultarProductosPorTipoProductoYClase(conexion, idPerfil, listadoDetalle.get(j).getIdTipoProducto(), listadoDetalle.get(j).getAgrupadorClase());
                                    productosAcumulados = productosAcumulados + productos;
                                    if (productos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    } else if (productosAcumulados >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    }
                                    // si requisito aprobado = true ; break;
                                    // no dar vuelta ,mantener array
                                }
                            }
                            if (requisitoAprobado == true) {
                                requisitoContador++;
//                            requisitoAprobado = false;
                                break;
                            }
                        }

                    } else {
                        if (requisitoAprobado == false) {
                            break;
                        }
                    }

                }
                if (requisitoAprobado == true) {
                    nivelInvestigador = "4";
                }
            }

            /// AQUI PARA ABAJO LO QUE HACE FALTA PARA EL SIQUIENTE NIVEL
            System.out.println("nivel del investigador ==rrr " + nivelInvestigador);
            listadoRequisitos = null;
            listadoDetalle = null;
            String desNivel = "";
            switch (nivelInvestigador) {
                case "2":
                    // no hay que consultar nada.
                    System.out.println("ya esta en el nivel superior == senior");
                    desNivel = "SENIOR";
                    break;
                case "3":
                    // consultar Requisitos del # 2
                    System.out.println(" estas en el nivel de === Asociado ");
                    desNivel = "ASOCIADO";
                    listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "2");
                    break;
                case "4":
                    // consultar Requisitos del # 3
                    System.out.println(" esta en el nivel de == junior");
                    desNivel = "JUNIOR";
                    listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "3");

                    break;
                case "":
                    System.out.println("No tienen ningun nivel :( ");
                    // consultar Requisitos del # 4
                    desNivel = "TE FALTA PARA OBTENER UN ESCALAFON";
                    listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "4");
                    break;
            }

            if (listadoRequisitos != null) {

                System.out.println("entro aquiiiiiiiiiiiiiiiiiiiiii a comprobar");
                boolean cumple = false;
                // recoccer requisitos del nivel a completar.
                listadoDetalleTemporal = new ArrayList<>();
                listadoDetalleFaltante = new ArrayList<>();
                productos = 0;
                productosAcumulados = 0;
                subProductos = 0;
                subProductosAcumulado = 0;
                requisitoAprobado = false;
                requisitoContador = 1;

                for (int i = 0; i < listadoRequisitos.size(); i++) {
                    System.out.println("vuelta del listado requisito ----- " + i);
                    System.out.println(" TIPO DE REQUISITO  " + listadoRequisitos.get(i).getIdTipoRequisito());
                    System.out.println(" contador requisito===========  " + requisitoContador);

                    if (Integer.parseInt(listadoRequisitos.get(i).getIdTipoRequisito()) == (requisitoContador)) {

                    } else {
                        if (requisitoAprobado == false) {

                            for (int j = 0; j < listadoDetalleTemporal.size(); j++) {
                                listadoDetalleFaltante.add(listadoDetalleTemporal.get(j));

                            }
                            listadoDetalleTemporal = null;
                            listadoDetalleTemporal = new ArrayList<>();
                            requisitoContador++;
                            productosAcumulados = 0;
                            subProductosAcumulado = 0;

//                            break;
                        }
                    }

                    System.out.println("ENTRO AL IF DE LOS REQUISITOSSSSSSSSSSS");

                    requisitoAprobado = false;

                    listadoDetalle = new DetalleRequisitoDAO().consultarDetalleRequisitosPorIdRequisito(conexion, listadoRequisitos.get(i).getId());

                    // recorrer el listado //
                    for (int j = 0; j < listadoDetalle.size(); j++) {

                        System.out.println("vuelta numero  del listado detalle -------- " + j);

                        System.out.println("detalle del requisito " + listadoDetalle.get(j).toStringJson());

                        if (listadoDetalle.get(j).getIdTipoValida().equals("1")) {

                            // validar por formacion academica
                            if (datosFormacion != null) {
                                for (int k = 0; k < datosFormacion.size(); k++) {
                                    if (listadoDetalle.get(j).getIdNivelFormacion().equals(datosFormacion.get(k).getNivelID())) {
                                        requisitoAprobado = true;
                                        break;
                                    } else {
                                        // agregar a listado temporal // 
                                        listadoDetalle.get(j).setDescripcionRequisito(listadoRequisitos.get(i).getIdTipoRequisito());
                                        if (listadoDetalleTemporal == null) {
                                            listadoDetalleTemporal = new ArrayList<>();
                                        }
                                        listadoDetalleTemporal.add(listadoDetalle.get(j));
                                    }
                                }
                            } else {
                                System.out.println("agregaria al listado temporal");
                                listadoDetalle.get(j).setDescripcionRequisito(listadoRequisitos.get(i).getIdTipoRequisito());
                                if (listadoDetalleTemporal == null) {
                                    listadoDetalleTemporal = new ArrayList<>();
                                }
                                listadoDetalleTemporal.add(listadoDetalle.get(j));
                                System.out.println("listado " + listadoDetalleTemporal.get(0).toStringJson());
                            }

                        } else if (listadoDetalle.get(j).getIdTipoValida().equals("2")) {
                            if (listadoRequisitos.get(i).getIdTipoRequisito().equals("3")) {
                                System.out.println("aqui entra por el requisito ### 3");
                                // consultar sub-productos del requisito 3
                                subProductos = new PerfilProductoDAO().consultarSubtipoPorId(conexion, idPerfil, listadoDetalle.get(j).getIdSubtipoProducto());
                                System.out.println("sub productos >>>>>>>>>>>>>>>>>>>>>>  " + subProductos);
                                if (subProductos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;
//                                } else if (subProductosAcumulado >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
//                                    requisitoAprobado = true;
//
                                } else {

                                    System.out.println("entro agregar sub productos al array  ");
                                    int dif = Integer.parseInt(listadoDetalle.get(j).getCantidad()) - subProductos;
                                    listadoDetalle.get(j).setCantidad(String.valueOf(dif));
                                    listadoDetalle.get(j).setDescripcionRequisito(listadoRequisitos.get(i).getIdTipoRequisito());
                                    if (listadoDetalleTemporal == null) {
                                        listadoDetalleTemporal = new ArrayList<>();
                                    }
                                    listadoDetalleTemporal.add(listadoDetalle.get(j));
                                }

                            } else {

                                // consultar productos dependiento el detalle 
                                productos = new PerfilProductoDAO().consultarProductosPorTipoProductoYClase(conexion, idPerfil, listadoDetalle.get(j).getIdTipoProducto(), listadoDetalle.get(j).getAgrupadorClase());
                                productosAcumulados = productosAcumulados + productos;
                                System.out.println("");
                                System.out.println("PRODUCTOS ENCONTRADOS  " + productos);
                                System.out.println("PRODUCTOS ACU   " + productosAcumulados);
                                System.out.println("");
                                if (productos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;

                                } else if (productosAcumulados >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;

                                } else {
                                    System.out.println("entro a guardar productoss faltantes ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo   ");
                                    int dif = Integer.parseInt(listadoDetalle.get(j).getCantidad()) - productos;
                                    listadoDetalle.get(j).setCantidad(String.valueOf(dif));
                                    listadoDetalle.get(j).setDescripcionRequisito(listadoRequisitos.get(i).getIdTipoRequisito());

                                    if (listadoDetalleTemporal == null) {
                                        listadoDetalleTemporal = new ArrayList<>();
                                    }
                                    listadoDetalleTemporal.add(listadoDetalle.get(j));
                                }
                                // si requisito aprobado = true ; break;
                                // no dar vuelta ,mantener array
                            }
                        }
                        if (requisitoAprobado == true) {
                            System.out.println("####################################################################################################################");
                            requisitoContador++;

                            listadoDetalleTemporal = null;
//                            requisitoAprobado = false;
                            break;
                        }
                    }

                }

            }
            if (requisitoAprobado == false) {

                for (int j = 0; j < listadoDetalleTemporal.size(); j++) {
                    listadoDetalleFaltante.add(listadoDetalleTemporal.get(j));

                }
                listadoDetalleTemporal = null;
                listadoDetalleTemporal = new ArrayList<>();
                requisitoContador++;
                productosAcumulados = 0;
                subProductosAcumulado = 0;

//                            break;
            }
            /// CREAR ARCHIVO DE PRODUCTOS FALTANTES.
            System.out.println("listado faltante5555 ====");
            String observacion = "";
            String control = "";
            System.out.println("listado !!!!!!!!!!!!!!!!!!!!!!! " + listadoDetalleFaltante);

            if (listadoDetalleFaltante != null) {

                System.out.println("Entro al lisssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
                for (int i = 0; i < listadoDetalleFaltante.size(); i++) {
                    DetalleRequisitoDTO inf = listadoDetalleFaltante.get(i);
                    if (inf.getDescripcionRequisito().equals(control)) {
                        String cuerpo = "";
                        switch (inf.getDescripcionRequisito()) {
                            case "1":
                                if (inf.getIdTipoValida().equals("1")) {
                                    cuerpo = inf.getCantidad() + " " + inf.getDescripcionNivelFormacion() + ",";
                                    observacion = observacion + cuerpo;
                                } else {
                                    if (inf.getIdTipoValida().equals("2")) {
                                        cuerpo = inf.getCantidad() + " " + inf.getDescripcionTipoProducto() + " CLASE " + inf.getAgrupadorClase() + ",";
                                        observacion = observacion + cuerpo;
                                    }
                                }
                                break;
                            case "2":
                                cuerpo = inf.getCantidad() + " " + inf.getDescripcionTipoProducto() + " CLASE " + inf.getAgrupadorClase() + ",";
                                observacion = observacion + cuerpo;
                                break;
                            case "3":
                                cuerpo = inf.getCantidad() + " " + inf.getDescripcionSubtipoProducto() + ",";
                                observacion = observacion + cuerpo;
                                break;
                        }

                    } else {
                        String titulo = "";
                        if (control.equals("")) {
                            titulo = "REQUISITO " + inf.getDescripcionRequisito() + "|";
                        } else {
                            titulo = "|REQUISITO " + inf.getDescripcionRequisito() + "|";
                        }
//                        String titulo = "|REQUISITO " + inf.getDescripcionRequisito() + "|";
                        observacion = observacion + titulo;
                        control = inf.getDescripcionRequisito();
                    }
                    System.out.println(listadoDetalleFaltante.get(i).toStringJson());
                }
            }

            System.out.println("OBSERVACION   " + observacion);
            System.out.println("Nivel investigador  " + nivelInvestigador);

            // actualizar perfil
            registroExitoso = new PerfilDAO().actualizarPerfilDesdeConsultarNivel(conexion, idPerfil, nivelInvestigador, observacion);

            if (registroExitoso == true) {

                if (nivelInvestigador == "2") {
                    respuesta = new RespuestaNivelDTO();
                    respuesta.setPerfil(idPerfil);
                    respuesta.setIdNivel(nivelInvestigador);
                    respuesta.setObservacion("Ya estas en el nivel Senior|Ninguno");
                    respuesta.setDescripcionNivel(desNivel);
                } else {
                    respuesta = new RespuestaNivelDTO();
                    respuesta.setPerfil(idPerfil);
                    respuesta.setIdNivel(nivelInvestigador);
                    respuesta.setObservacion(observacion);
                    respuesta.setDescripcionNivel(desNivel);
                }

            }

            conexion.close();
            conexion = null;
//            registroExitoso = true;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return respuesta;
    }

    /**
     *
     * @param idPerfil
     * @return
     */
    public RespuestaNivelDTO verNivelInvestigadorSimulador(String idPerfil) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;
        ArrayList<RequisitoTipoInvestigadorDTO> listadoRequisitos = null;
        ArrayList<DetalleRequisitoDTO> listadoDetalle = null;
        ArrayList<FormacionAcademicalDTO> datosFormacion = null;
        ArrayList<DetalleRequisitoDTO> listadoDetalleFaltante = null;
        ArrayList<DetalleRequisitoDTO> listadoDetalleTemporal;
        listadoDetalleTemporal = new ArrayList<DetalleRequisitoDTO>();
        DetalleRequisitoDTO datosFaltante = null;
        RespuestaNivelDTO respuesta = null;
        int requisitoContador = 1;
        boolean requisitoAprobado = false;
        int productos = 0;
        int productosAcumulados = 0;
        int subProductos = 0;
        int subProductosAcumulado = 0;
        String nivelInvestigador = "";

        try {
            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            // consultar nivel de formacion del perfil
            System.out.println("id perfil " + idPerfil);
            datosFormacion = new FormacionAcademicalDAO().consultarFormacionPorIdPerfilTemporal(conexion, idPerfil);//cambia se consulta tabla temporal y nivel de formacion diferente de null
            System.out.println("datos formacion " + datosFormacion);

            // consultar requisitos de senior
            listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "2");

            for (int i = 0; i < listadoRequisitos.size(); i++) {

                System.out.println(" TIPO DE REQUISITO  " + listadoRequisitos.get(i).getIdTipoRequisito());

                if (Integer.parseInt(listadoRequisitos.get(i).getIdTipoRequisito()) == (requisitoContador)) {

                    requisitoAprobado = false;

                    listadoDetalle = new DetalleRequisitoDAO().consultarDetalleRequisitosPorIdRequisito(conexion, listadoRequisitos.get(i).getId());

                    // recorrer el listado //
                    for (int j = 0; j < listadoDetalle.size(); j++) {

                        System.out.println(" detalle del requisito>>>>> " + listadoDetalle.get(j).toStringJson());

                        if (listadoDetalle.get(j).getIdTipoValida().equals("1")) {

                            System.out.println("tipon de validacion por formacion ");
                            // validar por formacion academica
                            if (datosFormacion != null) {
                                System.out.println(" datos de formacion diferente de null");
                                for (int k = 0; k < datosFormacion.size(); k++) {
                                    if (listadoDetalle.get(j).getIdNivelFormacion().equals(datosFormacion.get(k).getNivelID())) {
                                        System.out.println("cumplee la formacion");
                                        requisitoAprobado = true;
                                        break;
                                    }
                                    System.out.println("no cumple ");
                                }
                            }

                        } else if (listadoDetalle.get(j).getIdTipoValida().equals("2")) {
                            if (listadoRequisitos.get(i).getIdTipoRequisito().equals("3")) {
                                // consultar sub-productos del requisito 3
                                subProductos = new PerfilProductoDAO().consultarSubtipoPorIdTemporal(conexion, idPerfil, listadoDetalle.get(j).getIdSubtipoProducto());
                                //cambia se consulta tabla temporal por subtipo producto
                                if (subProductos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;
                                }
//                            else if (subProductosAcumulado >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
//                                requisitoAprobado = true;
//
//                            }
                            } else {

                                // consultar productos dependiento el detalle 
                                productos = new PerfilProductoDAO().consultarProductosPorTipoProductoYClaseTemporal(conexion, idPerfil, listadoDetalle.get(j).getIdTipoProducto(), listadoDetalle.get(j).getAgrupadorClase());
                                //cambia se consulta tabla temporal por ProductosPorTipoProductoYClase
                                productosAcumulados = productosAcumulados + productos;
                                if (productos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;

                                } else if (productosAcumulados >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;

                                }
                                // si requisito aprobado = true ; break;
                                // no dar vuelta ,mantener array
                            }
                        }
                        if (requisitoAprobado == true) {
                            requisitoContador++;
//                            requisitoAprobado = false;
                            break;
                        }

                    }

                } else {
                    if (requisitoAprobado == false) {
                        break;
                    }

                }
            }
            if (requisitoAprobado == true) {
                nivelInvestigador = "2";
            }

            System.out.println("reqiosito ´probado ==== " + requisitoAprobado);
            System.out.println(" nivel de investigador ==== " + nivelInvestigador);

            if (requisitoAprobado == false && nivelInvestigador == "") {

                // consultar requisitos de asociado
                listadoRequisitos = null;
                requisitoAprobado = false;
                requisitoContador = 1;
                productos = 0;
                productosAcumulados = 0;
                subProductos = 0;
                subProductosAcumulado = 0;
                listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "3");

                for (int i = 0; i < listadoRequisitos.size(); i++) {

                    if (Integer.parseInt(listadoRequisitos.get(i).getIdTipoRequisito()) == (requisitoContador)) {

                        requisitoAprobado = false;

                        listadoDetalle = new DetalleRequisitoDAO().consultarDetalleRequisitosPorIdRequisito(conexion, listadoRequisitos.get(i).getId());

                        // recorrer el listado //
                        for (int j = 0; j < listadoDetalle.size(); j++) {

                            if (listadoDetalle.get(j).getIdTipoValida().equals("1")) {

                                // validar por formacion academica
                                if (datosFormacion != null) {
                                    for (int k = 0; k < datosFormacion.size(); k++) {
                                        if (listadoDetalle.get(j).getIdNivelFormacion().equals(datosFormacion.get(k).getNivelID())) {
                                            requisitoAprobado = true;
                                            break;
                                        }
                                    }
                                }

                            } else if (listadoDetalle.get(j).getIdTipoValida().equals("2")) {

                                if (listadoRequisitos.get(i).getIdTipoRequisito().equals("3")) {
                                    // consultar sub-productos del requisito 3
                                    subProductos = new PerfilProductoDAO().consultarSubtipoPorIdTemporal(conexion, idPerfil, listadoDetalle.get(j).getIdSubtipoProducto());
                                    //cambia se consulta tabla temporal por subtipo producto
                                    if (subProductos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;
                                    } else if (subProductosAcumulado >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;
                                    }
                                } else {

                                    // consultar productos dependiento el detalle 
                                    productos = new PerfilProductoDAO().consultarProductosPorTipoProductoYClaseTemporal(conexion, idPerfil, listadoDetalle.get(j).getIdTipoProducto(), listadoDetalle.get(j).getAgrupadorClase());
                                    //cambia se consulta tabla temporal por ProductosPorTipoProductoYClase
                                    productosAcumulados = productosAcumulados + productos;
                                    if (productos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    } else if (productosAcumulados >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    }
                                    // si requisito aprobado = true ; break;
                                    // no dar vuelta ,mantener array
                                }
                            }

                            if (requisitoAprobado == true) {
                                requisitoContador++;
//                            requisitoAprobado = false;
                                break;
                            }

                        }

                    } else {
                        if (requisitoAprobado == false) {
                            break;
                        }

                    }

                }

                if (requisitoAprobado == true) {
                    nivelInvestigador = "3";
                }

            }

            if (requisitoAprobado == false) {

                // consultar requisitos de asociado
                listadoRequisitos = null;
                requisitoAprobado = false;
                requisitoContador = 1;
                productos = 0;
                productosAcumulados = 0;
                subProductos = 0;
                subProductosAcumulado = 0;
                listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "4");

                for (int i = 0; i < listadoRequisitos.size(); i++) {

                    if (Integer.parseInt(listadoRequisitos.get(i).getIdTipoRequisito()) == (requisitoContador)) {

                        requisitoAprobado = false;

                        listadoDetalle = new DetalleRequisitoDAO().consultarDetalleRequisitosPorIdRequisito(conexion, listadoRequisitos.get(i).getId());

                        // recorrer el listado //
                        for (int j = 0; j < listadoDetalle.size(); j++) {

                            if (listadoDetalle.get(j).getIdTipoValida().equals("1")) {

                                // validar por formacion academica
                                if (datosFormacion != null) {
                                    for (int k = 0; k < datosFormacion.size(); k++) {
                                        if (listadoDetalle.get(j).getIdNivelFormacion().equals(datosFormacion.get(k).getNivelID())) {
                                            requisitoAprobado = true;
                                            break;
                                        }
                                    }
                                }

                            } else if (listadoDetalle.get(j).getIdTipoValida().equals("2")) {
                                if (listadoRequisitos.get(i).getIdTipoRequisito().equals("3")) {
                                    // consultar sub-productos del requisito 3
                                    subProductos = new PerfilProductoDAO().consultarSubtipoPorIdTemporal(conexion, idPerfil, listadoDetalle.get(j).getIdSubtipoProducto());
                                    //cambia se consulta tabla temporal por subtipo producto
                                    if (subProductos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;
                                    } else if (subProductosAcumulado >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    }

                                } else {

                                    // consultar productos dependiento el detalle 
                                    productos = new PerfilProductoDAO().consultarProductosPorTipoProductoYClaseTemporal(conexion, idPerfil, listadoDetalle.get(j).getIdTipoProducto(), listadoDetalle.get(j).getAgrupadorClase());
                                    //cambia se consulta tabla temporal por ProductosPorTipoProductoYClase
                                    productosAcumulados = productosAcumulados + productos;
                                    if (productos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    } else if (productosAcumulados >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                        requisitoAprobado = true;

                                    }
                                    // si requisito aprobado = true ; break;
                                    // no dar vuelta ,mantener array
                                }
                            }
                            if (requisitoAprobado == true) {
                                requisitoContador++;
//                            requisitoAprobado = false;
                                break;
                            }
                        }

                    } else {
                        if (requisitoAprobado == false) {
                            break;
                        }
                    }

                }
                if (requisitoAprobado == true) {
                    nivelInvestigador = "4";
                }
            }

            /// AQUI PARA ABAJO LO QUE HACE FALTA PARA EL SIQUIENTE NIVEL
            System.out.println("nivel del investigador == " + nivelInvestigador);
            listadoRequisitos = null;
            listadoDetalle = null;
            String desNivel = "";
            switch (nivelInvestigador) {
                case "2":
                    // no hay que consultar nada.
                    System.out.println("ya esta en el nivel superior == senior");
                    desNivel = "SENIOR";

                    break;
                case "3":
                    // consultar Requisitos del # 2
                    System.out.println(" estas en el nivel de === Asociado ");
                    desNivel = "ASOCIADO";
                    listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "2");
                    break;
                case "4":
                    // consultar Requisitos del # 3
                    System.out.println(" esta en el nivel de == junior");
                    desNivel = "JUNIOR";
                    listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "3");

                    break;
                case "":
                    System.out.println("No tienen ningun nivel :( ");
                    // consultar Requisitos del # 4
                    desNivel = "TE FALTA PARA OBTENER UN ESCALAFON";
                    listadoRequisitos = new RequisitoTipoInvestigadorDAO().consultarRequisitosPorSubtipoInvestiador(conexion, "4");
                    break;
            }

            if (listadoRequisitos != null) {

                System.out.println("entro aquiiiiiiiiiiiiiiiiiiiiii a comprobar");
                boolean cumple = false;
                // recoccer requisitos del nivel a completar.
                listadoDetalleTemporal = new ArrayList<>();
                listadoDetalleFaltante = new ArrayList<>();
                productos = 0;
                productosAcumulados = 0;
                subProductos = 0;
                subProductosAcumulado = 0;
                requisitoAprobado = false;
                requisitoContador = 1;

                for (int i = 0; i < listadoRequisitos.size(); i++) {
                    System.out.println("vuelta del listado requisito ----- " + i);
                    System.out.println(" TIPO DE REQUISITO  " + listadoRequisitos.get(i).getIdTipoRequisito());
                    System.out.println(" contador requisito===========  " + requisitoContador);

                    if (Integer.parseInt(listadoRequisitos.get(i).getIdTipoRequisito()) == (requisitoContador)) {

                    } else {
                        if (requisitoAprobado == false) {

                            for (int j = 0; j < listadoDetalleTemporal.size(); j++) {
                                listadoDetalleFaltante.add(listadoDetalleTemporal.get(j));

                            }
                            //listadoDetalleTemporal = null;
                            listadoDetalleTemporal = new ArrayList<>();
                            requisitoContador++;
                            productosAcumulados = 0;
                            subProductosAcumulado = 0;

//                            break;
                        }
                    }

                    System.out.println("ENTRO AL IF DE LOS REQUISITOSSSSSSSSSSS");

                    requisitoAprobado = false;

                    listadoDetalle = new DetalleRequisitoDAO().consultarDetalleRequisitosPorIdRequisito(conexion, listadoRequisitos.get(i).getId());

                    // recorrer el listado //
                    for (int j = 0; j < listadoDetalle.size(); j++) {

                        System.out.println("vuelta numero  del listado detalle -------- " + j);

                        System.out.println("detalle del requisito " + listadoDetalle.get(j).toStringJson());

                        if (listadoDetalle.get(j).getIdTipoValida().equals("1")) {

                            // validar por formacion academica
                            if (datosFormacion != null) {
                                for (int k = 0; k < datosFormacion.size(); k++) {
                                    if (listadoDetalle.get(j).getIdNivelFormacion().equals(datosFormacion.get(k).getNivelID())) {
                                        requisitoAprobado = true;
                                        break;
                                    } else {
                                        // agregar a listado temporal // 
                                        listadoDetalle.get(j).setDescripcionRequisito(listadoRequisitos.get(i).getIdTipoRequisito());
                                        listadoDetalleTemporal.add(listadoDetalle.get(j));
                                    }
                                }
                            } else {
                                System.out.println("agregaria al listado temporal");
                                listadoDetalle.get(j).setDescripcionRequisito(listadoRequisitos.get(i).getIdTipoRequisito());
                                listadoDetalleTemporal.add(listadoDetalle.get(j));
                                System.out.println("listado " + listadoDetalleTemporal.get(0).toStringJson());
                            }

                        } else if (listadoDetalle.get(j).getIdTipoValida().equals("2")) {
                            if (listadoRequisitos.get(i).getIdTipoRequisito().equals("3")) {
                                System.out.println("aqui entra por el requisito ### 3");
                                // consultar sub-productos del requisito 3
                                subProductos = new PerfilProductoDAO().consultarSubtipoPorIdTemporal(conexion, idPerfil, listadoDetalle.get(j).getIdSubtipoProducto());
                                System.out.println("sub productos >>>>>>>>>>>>>>>>>>>>>>  " + subProductos);
                                if (subProductos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;
//                                } else if (subProductosAcumulado >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
//                                    requisitoAprobado = true;
//
                                } else {

                                    System.out.println("entro agregar sub productos al array  ");
                                    int dif = Integer.parseInt(listadoDetalle.get(j).getCantidad()) - subProductos;
                                    listadoDetalle.get(j).setCantidad(String.valueOf(dif));
                                    listadoDetalle.get(j).setDescripcionRequisito(listadoRequisitos.get(i).getIdTipoRequisito());
                                    listadoDetalleTemporal.add(listadoDetalle.get(j));
                                }

                            } else {

                                // consultar productos dependiento el detalle 
                                productos = new PerfilProductoDAO().consultarProductosPorTipoProductoYClaseTemporal(conexion, idPerfil, listadoDetalle.get(j).getIdTipoProducto(), listadoDetalle.get(j).getAgrupadorClase());
                                productosAcumulados = productosAcumulados + productos;
                                System.out.println("");

                                System.out.println("PRODUCTOS ENCONTRADOS  " + productos);

                                System.out.println("PRODUCTOS ACU   " + productosAcumulados);
                                System.out.println("");
                                if (productos >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;

                                } else if (productosAcumulados >= Integer.parseInt(listadoDetalle.get(j).getCantidad())) {
                                    requisitoAprobado = true;

                                } else {
                                    System.out.println("entro a guardar productoss faltantes --------------------------------------   ");
                                    int dif = Integer.parseInt(listadoDetalle.get(j).getCantidad()) - productos;
                                    listadoDetalle.get(j).setCantidad(String.valueOf(dif));
                                    listadoDetalle.get(j).setDescripcionRequisito(listadoRequisitos.get(i).getIdTipoRequisito());

                                    System.out.println("xc:" + listadoDetalle.get(j).toStringJson());
                                    listadoDetalleTemporal.add(listadoDetalle.get(j));
                                }
                                // si requisito aprobado = true ; break;
                                // no dar vuelta ,mantener array
                            }
                        }
                        if (requisitoAprobado == true) {
                            System.out.println("####################################################################################################################");
                            requisitoContador++;

                            listadoDetalleTemporal = new ArrayList<>();
//                            requisitoAprobado = false;
                            break;
                        }
                    }

                }

            }
            if (requisitoAprobado == false) {

                for (int j = 0; j < listadoDetalleTemporal.size(); j++) {
                    listadoDetalleFaltante.add(listadoDetalleTemporal.get(j));

                }
                //listadoDetalleTemporal = null;
                listadoDetalleTemporal = new ArrayList<>();
                requisitoContador++;
                productosAcumulados = 0;
                subProductosAcumulado = 0;

//                            break;
            }
            /// CREAR ARCHIVO DE PRODUCTOS FALTANTES.
            System.out.println("listado faltante ====");
            String observacion = "";
            String control = "";
            if (listadoDetalleFaltante != null) {
                for (int i = 0; i < listadoDetalleFaltante.size(); i++) {
                    DetalleRequisitoDTO inf = listadoDetalleFaltante.get(i);
                    if (inf.getDescripcionRequisito().equals(control)) {
                        String cuerpo = "";
                        switch (inf.getDescripcionRequisito()) {
                            case "1":
                                if (inf.getIdTipoValida().equals("1")) {
                                    cuerpo = inf.getCantidad() + " " + inf.getDescripcionNivelFormacion() + ",";
                                    observacion = observacion + cuerpo;
                                } else {
                                    if (inf.getIdTipoValida().equals("2")) {
                                        cuerpo = inf.getCantidad() + " " + inf.getDescripcionTipoProducto() + " CLASE " + inf.getAgrupadorClase() + ",";
                                        observacion = observacion + cuerpo;
                                    }
                                }
                                break;
                            case "2":
                                cuerpo = inf.getCantidad() + " " + inf.getDescripcionTipoProducto() + " CLASE " + inf.getAgrupadorClase() + ",";
                                observacion = observacion + cuerpo;
                                break;
                            case "3":
                                cuerpo = inf.getCantidad() + " " + inf.getDescripcionSubtipoProducto() + ",";
                                observacion = observacion + cuerpo;
                                break;
                        }

                    } else {
                        String titulo = "";
                        if (control.equals("")) {
                            titulo = "REQUISITO " + inf.getDescripcionRequisito() + "|";
                        } else {
                            titulo = "|REQUISITO " + inf.getDescripcionRequisito() + "|";
                        }
//                        String titulo = "|REQUISITO " + inf.getDescripcionRequisito() + "|";
                        observacion = observacion + titulo;
                        control = inf.getDescripcionRequisito();
                    }
                    System.out.println(listadoDetalleFaltante.get(i).toStringJson());
                }
            }

            System.out.println("OBSERVACION   " + observacion);
            System.out.println("Nivel investigador  " + nivelInvestigador);

            // actualizar perfil
            registroExitoso = new PerfilDAO().actualizarPerfilDesdeConsultarNivel(conexion, idPerfil, nivelInvestigador, observacion);
            if (registroExitoso == true) {
                if (nivelInvestigador == "2") {
                    respuesta = new RespuestaNivelDTO();
                    respuesta.setPerfil(idPerfil);
                    respuesta.setIdNivel(nivelInvestigador);
                    respuesta.setObservacion("Ya estas en el nivel Senior|Ninguno");
                    respuesta.setDescripcionNivel(desNivel);
                }/* else if (nivelInvestigador == "3") {
                    respuesta = new RespuestaNivelDTO();
                    respuesta.setPerfil(idPerfil);
                    respuesta.setIdNivel(nivelInvestigador);
                    respuesta.setObservacion("Ya estas en el nivel Asociado|Ninguno");
                    respuesta.setDescripcionNivel(desNivel);
                }else if (nivelInvestigador == "4") {
                    respuesta = new RespuestaNivelDTO();
                    respuesta.setPerfil(idPerfil);
                    respuesta.setIdNivel(nivelInvestigador);
                    respuesta.setObservacion("Ya estas en el nivel junior|Ninguno");
                    respuesta.setDescripcionNivel(desNivel);
                }*/ else {
                    respuesta = new RespuestaNivelDTO();
                    respuesta.setPerfil(idPerfil);
                    respuesta.setIdNivel(nivelInvestigador);
                    respuesta.setObservacion(observacion);
                    respuesta.setDescripcionNivel(desNivel);
                }

            } else {
                respuesta = new RespuestaNivelDTO();
                respuesta.setPerfil(idPerfil);
                respuesta.setIdNivel(nivelInvestigador);
                respuesta.setObservacion(observacion);
                respuesta.setDescripcionNivel(desNivel);
            }

            conexion.close();
            conexion = null;
//            registroExitoso = true;
        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return respuesta;
    }

    /**
     *
     * @param formacion
     * @return
     */
    public boolean registrarInfAcademicaSimulacion(FormacionAcademicalDTO formacion) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean registroExitoso = true;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");
            formacion.setPerfilID(usuarioLogueado.getIdPerfil());
            if (!new FormacionAcademicalDAO().SimulacionregistrarFormacion(conexion, formacion)) {
                throw new Exception("Error al registrar simulación formación academica.");

            }
            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @param Datos
     * @return
     */
    public boolean registrarPerfilProductoSimulacion(PerfilProductoDTO Datos) {

        DataBaseConnection dbcon = null;
        Connection conexion = null;

        boolean registroExitoso = true;

        try {
            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            Datos.setIdPerfil(usuarioLogueado.getIdPerfil());
            if (!new PerfilProductoDAO().registrarPerfilProductoSimulacion(conexion, Datos)) {
                throw new Exception("Error al registrar simulacion perfil producto.");

            }
            registroExitoso = true;

        } catch (Exception e) {
            registroExitoso = false;
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

    /**
     *
     * @return
     */
    public ArrayList<ProgramaAcademicoDTO> listarProgramaAcademico() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<ProgramaAcademicoDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new ProgramaAcademicoDAO().listarProgramaAcademico(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     *
     * @return
     */
    public ArrayList<TipoProfesorDTO> listarTipoProfesor() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList<TipoProfesorDTO> listado = null;

        try {

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new TipoProfesorDAO().listarTipoProfesor(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }

        return listado;
    }

    /**
     * @return
     */
    public ArrayList<PerfilDTO> listarUsuariosInvestigadores() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        ArrayList listado = null;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);

            listado = new PerfilDAO().listarUsuariosInvestigadores(conexion);

            conexion.close();
            conexion = null;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return listado;
    }

    public boolean limpiarsimulador() {

        DataBaseConnection dbcon = null;
        Connection conexion = null;
        boolean registroExitoso = false;

        try {

            HttpSession session = WebContextFactory.get().getSession();
            PerfilDTO usuarioLogueado = (PerfilDTO) session.getAttribute("datosUsuario");

            dbcon = DataBaseConnection.getInstance();
            conexion = dbcon.getConnection(ContextDataResourceNames.MYSQL_GESTION_JDBC);
            if (!new PerfilDAO().limpiarsimulador(conexion, usuarioLogueado.getIdPerfil())) {
                throw new Exception("Error al limpiar simulador.");
            }

            registroExitoso = true;

        } catch (Exception e) {
            LoggerMessage.getInstancia().loggerMessageException(e);
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                    conexion = null;
                }
            } catch (Exception e) {
                LoggerMessage.getInstancia().loggerMessageException(e);
            }
        }
        return registroExitoso;
    }

}
