<%-- 
    Document   : editar-tipo-requisito
    Created on : 16/07/2018, 05:16:14 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link href="plugins/datatables/css/jquery.dataTables.css" rel="stylesheet">
<link href="plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">
<h1 class="page-title">Editar Nivel de Formaciòn</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="#">Nivel de Formaciòn</a></li> 
    <li class="active"><strong>Editar</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Editar</h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div id="alert"></div>
                <div class="table-responsive" id="divTabla">
                    <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable">
                        <thead>
                            <tr>
                                <th>Descripciòn</th>
                                <th>Estado</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody id="lisUsuario"></tbody>
                        <tfoot>
                            <tr>
                                <th>Descripciòn</th>
                                <th>Estado</th>
                                <th>Editar</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nombre">Descrpción</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Descrpción" required maxlength="50" title="Ingrese nombre">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="estado">Estado</label>
                            <select class="form-control" id="estado" name="estado" required title="Seleccione una opcion">
                                <option value="">Seleccione una opción</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>                     
                    </div>                  
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded" onclick="volver();">Volver</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formRegistrar', 1);">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="footer-main"> 
    &copy; 2016 <strong>Integral</strong> Admin Template by <a target="_blank" href="#/">G-axon</a>
</footer>	
<!-- /footer -->
<script src="plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables/js/jszip.min.js"></script>
<script src="plugins/datatables/js/pdfmake.min.js"></script>
<script src="plugins/datatables/js/vfs_fonts.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
                                var idOpcion;
                                var operacion;
                                $(document).ready(function () {
                                    listarNivelFormacion();
                                });

                                function volver() {
                                    $("#divTabla").show();
                                    $("#formRegistrar").hide();
                                }

                                function listarNivelFormacion() {
                                    ajaxGestion.listarNivelFormacion({
                                        callback: function (data) {
                                            if (data !== null) {
                                                listado = data;
                                                $('#myTable').dataTable().fnDestroy();
                                                dwr.util.removeAllRows("lisUsuario");
                                                dwr.util.addRows("lisUsuario", listado, mapa, {
                                                    escapeHtml: false
                                                });
                                                $(".table-responsive").show();
                                                $("#formRegistrar").hide();
                                                $('.dataTables-example').DataTable({
                                                    dom: '<"html5buttons" B>lTfgitp',
                                                    buttons: [
                                                        {
                                                            extend: 'copyHtml5',
                                                            exportOptions: {
                                                                columns: [0, ':visible']
                                                            }
                                                        },
                                                        {
                                                            extend: 'excelHtml5',
                                                            exportOptions: {
                                                                columns: ':visible'
                                                            }
                                                        },
                                                        {
                                                            extend: 'pdfHtml5',
                                                            exportOptions: {
                                                                columns: [0, 1, 2, 3, 4]
                                                            }
                                                        },
                                                        'colvis'
                                                    ]
                                                });
                                            } else {
                                                notificacion("alert-danger", "No se <strong>encontraron</strong> resultados.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

                                var listado = [];
                                var mapa = [
                                    function (data) {
                                        return data.descripcion;
                                    },
                                    function (data) {
                                        if (data.estado == "1") {
                                            return "Activo";
                                        } else {
                                            return "Inactivo";
                                        }
                                    },
                                    function (data) {
                                        return "<button class='btn btn-primary btn-rounded btn-xs' type='button'  onclick='verDatos(" + data.id + ");'>Editar</button>";
                                    }
                                ];

                                function ejecutarPostValidate() {
                                    if (operacion == 1)
                                        actualizarNivelFormacion();
                                    operacion = null;
                                }

                                function notificacion(t, m) {
                                    if ($('#alert').text() == "") {
                                        setTimeout('$("#alert").text("")', 3000);
                                    }
                                    $("#alert").text("");
                                    $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                                            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                            + '<span aria-hidden="true">×</span>'
                                            + '</button>'
                                            + m
                                            + '</div>');
                                    $("#alert").focus();

                                }

                                function verDatos(id) {
                                    for (var i = 0; i < listado.length; i++) {
                                        if (listado[i].id == id) {
                                            idOpcion = id;
                                            $("#nombre").val(listado[i].descripcion);
                                            $("#estado").val(listado[i].estado);
                                        }
                                    }
                                    $("#divTabla").hide();
                                    $("#formRegistrar").show();
                                }

                                function actualizarNivelFormacion() {

                                    var perfil = {
                                        id: idOpcion,
                                        estado: $("#estado").val(),
                                        descripcion: $("#nombre").val()
                                    };
                                    ajaxGestion.actualizarNivelFormacion(perfil, {
                                        callback: function (data) {
                                            console.log(data);
                                            if (data) {
                                                notificacion("alert-success", "Se <strong>registraron</strong> los datos on exíto.");
                                                listarNivelFormacion();
                                                volver();
                                            } else {
                                                notificacion("alert-danger", "Error al <strong>registrar</strong> los datos.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

</script>
