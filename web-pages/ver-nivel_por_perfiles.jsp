<%@page import="co.usb.gestion.mvc.dto.PerfilDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    PerfilDTO datosUsuario = (PerfilDTO) session.getAttribute("datosUsuario");

%>

<!DOCTYPE html>
<h1 class="page-title">Consultar Nivel Investigador</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <!--    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
        <li><a href="form-basic.html">Revisar</a></li> 
        <li class="active"><strong>Ver Nivel</strong></li> -->
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Nivel del Investigador:&nbsp;&nbsp;&nbsp;</h4>
                <h4 id="nivel" class="panel-title" style="color:#148e40"></h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">

                    <div class="row">  
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="subtipo">Investigador:</label>
                            <select class="form-control" id="perfil" name="perfil" title="Seleccione una opcion"></select>
                        </div>

                    </div> 

                    <table class="table">
                        <thead>
                            <tr>
                                <th># Requisitos</th>
                                <th>Productos Faltantes</th>                               
                            </tr>
                        </thead>
                        <tbody id="rd"></tbody>

                    </table>



                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="verNivel();">Ver Nivel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="footer-main"> 
    &copy; 2016 <strong>Integral</strong> Admin Template by <a target="_blank" href="#/">G-axon</a>
</footer>	
<!-- /footer -->
<script>
    $(document).ready(function () {
        listarUsuariosInvestigadores("perfil");
    <% System.out.println("datos " + datosUsuario.toStringJson());%>

    });

    function listarUsuariosInvestigadores(idCombo, valorSeleccionado) {
        ajaxGestion.listarUsuariosInvestigadores({
            callback: function (data) {
                console.log("data::: ", data);
                dwr.util.removeAllOptions(idCombo);
                dwr.util.addOptions(idCombo, [{
                        id: '',
                        nombre: 'Seleccione una opción'
                    }], 'id', 'nombre');
                if (data !== null) {
                    dwr.util.addOptions(idCombo, data, 'id', 'nombre');
                    $("#" + idCombo).val(valorSeleccionado).trigger("change");
                }
            },
            timeout: 20000
        });
    }



    function verNivel() {

        var perfil = $("#perfil").val();

        if (perfil == "") {
            alert("Seleccione un investigador");
            return;
        }

        ajaxGestion.verNivelInvestigador(perfil, {
            callback: function (data) {
                console.log(data);
                if (data.observacion != null) {

                    $("#rd").html("");
                    $("#nivel").html(data.descripcionNivel);
                    console.log("lo que llega ", data);


                    console.log("XXX", data.observacion);

                    var separador = "|";
                    var arregloDeSubCadenas = [];
                    var mostrar = data.observacion;
                    arregloDeSubCadenas = mostrar.split(separador);

                    console.log("XXX", arregloDeSubCadenas);





                    for (var i = 0; i < arregloDeSubCadenas.length; i++) {



                        console.log("R", arregloDeSubCadenas[i]);

                        console.log("D", arregloDeSubCadenas[i + 1]);

                        $("#rd").append("<tr><td>" + arregloDeSubCadenas[i] + "</td><td>" + arregloDeSubCadenas[i + 1] + "</td></tr>");

                        i++;

                    }



                    console.log(data);


                }
            }
        });

    }
</script>
