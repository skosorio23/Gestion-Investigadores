<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<h1 class="page-title">Registrar Tipo Requisito</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="form-basic.html">Tipo Requisito</a></li> 
    <li class="active"><strong>Registro</strong></li> 
</ol>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Registro</h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <form role="form" id="formRegistrar" action="return:false" autocomplete="off">
                    <div id="alert"></div>
                    <div class="row">                                            
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nombre">Descrpción</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Descrpción" required maxlength="50" title="Ingrese nombre">
                        </div>
                        <div class="form-group col-lg-6">                                                   
                            <label class="control-label" for="estado">Estado</label>
                            <select class="form-control" id="estado" name="estado" required title="Seleccione una opcion">
                                <option value="">Seleccione una opción</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>                     
                    </div>                  
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="reset" class="btn btn-white btn-rounded">Cancelar</button>
                            <button type="submit" class="btn btn-primary btn-rounded" onclick="validar('formRegistrar', 1);">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="footer-main"> 
    &copy; 2016 <strong>Integral</strong> Admin Template by <a target="_blank" href="#/">G-axon</a>
</footer>	
<!-- /footer -->
<script>
    var operacion = null;
    $(document).ready(function () {
    });

    $("input[type=tel]").on('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    function registrarTipoRequisito() {

        var datos = {
            estado: $("#estado").val(),
            descripcion: $("#nombre").val()
        };
        ajaxGestion.registrarTipoRequisito(datos, {
            callback: function (data) {
                console.log(data);
                if (data) {
                    notificacion("alert-success", "Se <strong>registraron</strong> los datos on exíto.");
                    $("#formRegistrar")[0].reset();
                } else {
                    notificacion("alert-danger", "Error al <strong>registrar</strong> los datos.");
                }
            },
            timeout: 20000
        });
    }

    function notificacion(t, m) {
        if ($('#alert').text() == "") {
            setTimeout('$("#alert").text("")', 3000);
        }
        $("#alert").text("");
        $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                + '<span aria-hidden="true">×</span>'
                + '</button>'
                + m
                + '</div>');
        $("#alert").focus();

    }

    function ejecutarPostValidate() {
        if (operacion == 1)
            registrarTipoRequisito();
        // if (operacion == 2)
        operacion = null;
    }

</script>
