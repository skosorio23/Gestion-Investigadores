<%-- 
    Document   : prueba
    Created on : 10/07/2018, 07:58:57 PM
    Author     : carlos
--%>

<%-- 
    Document   : perfiles-productos
    Created on : 6/07/2018, 11:00:29 AM
    Author     : shirley
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link href="plugins/datatables/css/jquery.dataTables.css" rel="stylesheet">
<link href="plugins/datatables/extensions/Buttons/css/buttons.dataTables.css" rel="stylesheet">

<h1 class="page-title">Ver perfil investigador</h1>
<!-- Breadcrumb -->
<ol class="breadcrumb breadcrumb-2"> 
    <li><a href="index.html"><i class="fa fa-home"></i>Home</a></li> 
    <li><a href="#">Perfil</a></li> 
    <li class="active"><strong>Investigador</strong></li> 
</ol>

<div class="row" id="div2">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Listado Productos</h4>
                <ul class="panel-tool-options"> 
                    <li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
                    <li><a data-rel="reload" href="#"><i class="icon-cw"></i></a></li>
                    <li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <div id="alert"></div>

                <div class="table-responsive" hidden >
                    <table class="table table-striped table-bordered table-hover dataTables-example" id="myTable1">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Fescripcion</th>
                                <th>Fecha</th>
                                <th>Subtipo Producto</th>
                                <th>Tipo Producto</th>
                            </tr>
                        </thead>
                        <tbody id="listadoProductos"></tbody>

                    </table>
                    <div class="line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-5">
                            <button type="button" class="btn btn-white btn-rounded" onclick="volverBuscar();">volver Buscar</button>                       
                        </div>
                    </div>
                </div>
                <br>                          
            </div>
        </div>
    </div>
</div>

<script src="plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables/js/jszip.min.js"></script>
<script src="plugins/datatables/js/pdfmake.min.js"></script>
<script src="plugins/datatables/js/vfs_fonts.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.html5.js"></script>
<script src="plugins/datatables/extensions/Buttons/js/buttons.colVis.js"></script>

<script>
                                $(document).ready(function () {
                                    listarProductoXIdPerfil(idPerfil);
                                });

                                var listadoProductos = [];
                                var mapaProductos = [
                                    function (data) {
                                        return data.documento;
                                    },
                                    function (data) {
                                        return data.correo;
                                    },
                                    function (data) {
                                        return data.tipoUsuario;
                                    },
                                    function (data) {
                                        return data.anioGraduacionFA;
                                    },
                                    function (data) {
                                        return data.institucionFA;
                                    }
                                ];

                                function listarProductoXIdPerfil(idPerfil) {

                                    ajaxGestion.ConsultarPerfilProducto(idPerfil, {
                                        callback: function (data) {
                                            console.log("----- ", data);
                                            if (data !== null) {
                                                listadoProductos = data;
                                                $('#myTable1').dataTable().fnDestroy();
                                                dwr.util.removeAllRows("listadoProductos");
                                                dwr.util.addRows("listadoProductos", listadoProductos, mapaProductos, {
                                                    escapeHtml: false
                                                });

                                                $("#div1").hide();
                                                //$("#formRegistrar").hide();
                                                $("#div2").show();

                                                $('#myTable1').DataTable({
                                                    dom: '<"html5buttons" B>lTfgitp',
                                                    buttons: [
                                                        {
                                                            extend: 'copyHtml5',
                                                            exportOptions: {
                                                                columns: [0, ':visible']
                                                            }
                                                        },
                                                        {
                                                            extend: 'excelHtml5',
                                                            exportOptions: {
                                                                columns: ':visible'
                                                            }
                                                        },
                                                        {
                                                            extend: 'pdfHtml5',
                                                            exportOptions: {
                                                                columns: [0, 1, 2, 3, 4]
                                                            }
                                                        },
                                                        'colvis'
                                                    ]
                                                });
                                            } else {
                                                notificacion("alert-danger", "No se <strong>encontraron</strong> resultados.");
                                            }
                                        },
                                        timeout: 20000
                                    });
                                }

                                //function ejecutarPostValidate() {
                                //if (operacion == 1)
                                //consultarGestionPerfilProducto();
                                // if (operacion == 2)
                                //operacion = null;
                                //}

                                function notificacion(t, m) {
                                    if ($('#alert').text() == "") {
                                        setTimeout('$("#alert").text("")', 3000);
                                    }
                                    $("#alert").text("");
                                    $("#alert").append('<div class="alert ' + t + ' alert-dismissible" role="alert">'
                                            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                            + '<span aria-hidden="true">×</span>'
                                            + '</button>'
                                            + m
                                            + '</div>');
                                    $("#alert").focus();

                                }

</script>